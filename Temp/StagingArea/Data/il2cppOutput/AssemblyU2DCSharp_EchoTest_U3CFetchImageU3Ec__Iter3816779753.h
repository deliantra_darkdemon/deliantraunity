﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// EchoTest
struct EchoTest_t3020015217;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EchoTest/<FetchImage>c__Iterator3
struct  U3CFetchImageU3Ec__Iterator3_t3816779753  : public Il2CppObject
{
public:
	// System.String EchoTest/<FetchImage>c__Iterator3::url
	String_t* ___url_0;
	// UnityEngine.WWW EchoTest/<FetchImage>c__Iterator3::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_1;
	// System.Int32 EchoTest/<FetchImage>c__Iterator3::index
	int32_t ___index_2;
	// EchoTest EchoTest/<FetchImage>c__Iterator3::$this
	EchoTest_t3020015217 * ___U24this_3;
	// System.Object EchoTest/<FetchImage>c__Iterator3::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean EchoTest/<FetchImage>c__Iterator3::$disposing
	bool ___U24disposing_5;
	// System.Int32 EchoTest/<FetchImage>c__Iterator3::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CFetchImageU3Ec__Iterator3_t3816779753, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFetchImageU3Ec__Iterator3_t3816779753, ___U3CwwwU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(U3CFetchImageU3Ec__Iterator3_t3816779753, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CFetchImageU3Ec__Iterator3_t3816779753, ___U24this_3)); }
	inline EchoTest_t3020015217 * get_U24this_3() const { return ___U24this_3; }
	inline EchoTest_t3020015217 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(EchoTest_t3020015217 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CFetchImageU3Ec__Iterator3_t3816779753, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CFetchImageU3Ec__Iterator3_t3816779753, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CFetchImageU3Ec__Iterator3_t3816779753, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
