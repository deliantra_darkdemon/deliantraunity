﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EchoTest/<FetchFacesJson>c__Iterator1
struct U3CFetchFacesJsonU3Ec__Iterator1_t4119425628;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EchoTest/<FetchFacesJson>c__Iterator1::.ctor()
extern "C"  void U3CFetchFacesJsonU3Ec__Iterator1__ctor_m3122957467 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EchoTest/<FetchFacesJson>c__Iterator1::MoveNext()
extern "C"  bool U3CFetchFacesJsonU3Ec__Iterator1_MoveNext_m3697603421 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EchoTest/<FetchFacesJson>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchFacesJsonU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2500433069 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EchoTest/<FetchFacesJson>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchFacesJsonU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3506911445 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest/<FetchFacesJson>c__Iterator1::Dispose()
extern "C"  void U3CFetchFacesJsonU3Ec__Iterator1_Dispose_m2411797382 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest/<FetchFacesJson>c__Iterator1::Reset()
extern "C"  void U3CFetchFacesJsonU3Ec__Iterator1_Reset_m240333128 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
