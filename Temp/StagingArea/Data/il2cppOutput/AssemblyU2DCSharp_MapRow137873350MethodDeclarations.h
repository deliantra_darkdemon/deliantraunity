﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapRow
struct MapRow_t137873350;
// MapCell
struct MapCell_t3263021842;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MapCell3263021842.h"

// System.Void MapRow::.ctor(System.Int32)
extern "C"  void MapRow__ctor_m3625560204 (MapRow_t137873350 * __this, int32_t ___destSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MapRow::get_length()
extern "C"  int32_t MapRow_get_length_m3503286366 (MapRow_t137873350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapRow::addCell(MapCell)
extern "C"  void MapRow_addCell_m354249166 (MapRow_t137873350 * __this, MapCell_t3263021842 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
