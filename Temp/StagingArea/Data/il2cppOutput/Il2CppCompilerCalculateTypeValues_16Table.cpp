﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_WebSocket1213274227.h"
#include "AssemblyU2DCSharpU2Dfirstpass_WebSocket_U3CConnectU738167129.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_CameraController3555666667.h"
#include "AssemblyU2DCSharp_MapCellData826838626.h"
#include "AssemblyU2DCSharp_MapCell3263021842.h"
#include "AssemblyU2DCSharp_MapRow137873350.h"
#include "AssemblyU2DCSharp_CMap632723465.h"
#include "AssemblyU2DCSharp_FacesJSONEntryObject727229337.h"
#include "AssemblyU2DCSharp_FacesJSONObject1129027889.h"
#include "AssemblyU2DCSharp_ReplyJSONDataArray1061980569.h"
#include "AssemblyU2DCSharp_EchoTest3020015217.h"
#include "AssemblyU2DCSharp_EchoTest_U3ConWSConnectedU3Ec__It570499943.h"
#include "AssemblyU2DCSharp_EchoTest_U3CFetchFacesJsonU3Ec__4119425628.h"
#include "AssemblyU2DCSharp_EchoTest_U3CFetchPacketsJsonU3Ec3529262072.h"
#include "AssemblyU2DCSharp_EchoTest_U3CFetchImageU3Ec__Iter3816779753.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_testWebsock1614810498.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1608[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1609[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1610[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1611[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1614[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1615[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1616[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1617[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1622[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1624[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1630[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (WebSocket_t1213274227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1633[2] = 
{
	WebSocket_t1213274227::get_offset_of_mUrl_0(),
	WebSocket_t1213274227::get_offset_of_m_NativeRef_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (U3CConnectU3Ec__Iterator0_t738167129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1634[5] = 
{
	U3CConnectU3Ec__Iterator0_t738167129::get_offset_of_U3CstringPtrU3E__0_0(),
	U3CConnectU3Ec__Iterator0_t738167129::get_offset_of_U24this_1(),
	U3CConnectU3Ec__Iterator0_t738167129::get_offset_of_U24current_2(),
	U3CConnectU3Ec__Iterator0_t738167129::get_offset_of_U24disposing_3(),
	U3CConnectU3Ec__Iterator0_t738167129::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (CameraController_t3555666667), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (MapCellData_t826838626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1637[8] = 
{
	MapCellData_t826838626::get_offset_of_darkness_0(),
	MapCellData_t826838626::get_offset_of_flags_1(),
	MapCellData_t826838626::get_offset_of_player_2(),
	MapCellData_t826838626::get_offset_of_tiles_3(),
	MapCellData_t826838626::get_offset_of_tilesCount_4(),
	MapCellData_t826838626::get_offset_of_stat_width_5(),
	MapCellData_t826838626::get_offset_of_stat_hp_6(),
	MapCellData_t826838626::get_offset_of_isDirty_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (MapCell_t3263021842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1638[3] = 
{
	MapCell_t3263021842::get_offset_of_data_0(),
	MapCell_t3263021842::get_offset_of_previous_1(),
	MapCell_t3263021842::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (MapRow_t137873350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1639[3] = 
{
	MapRow_t137873350::get_offset_of_cells_0(),
	MapRow_t137873350::get_offset_of_firstCell_1(),
	MapRow_t137873350::get_offset_of_lastCell_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (CMap_t632723465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1640[1] = 
{
	CMap_t632723465::get_offset_of_rows_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (FacesJSONEntryObject_t727229337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1641[2] = 
{
	FacesJSONEntryObject_t727229337::get_offset_of_id_0(),
	FacesJSONEntryObject_t727229337::get_offset_of_csum_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (FacesJSONObject_t1129027889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1642[1] = 
{
	FacesJSONObject_t1129027889::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (ReplyJSONDataArray_t1061980569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1643[1] = 
{
	ReplyJSONDataArray_t1061980569::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (EchoTest_t3020015217), -1, sizeof(EchoTest_t3020015217_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1644[21] = 
{
	EchoTest_t3020015217::get_offset_of_myTextField_2(),
	EchoTest_t3020015217::get_offset_of_emptySprite_3(),
	EchoTest_t3020015217::get_offset_of_baseURL_4(),
	EchoTest_t3020015217::get_offset_of_offset_5(),
	EchoTest_t3020015217::get_offset_of_extiNextId_6(),
	EchoTest_t3020015217::get_offset_of_lines_7(),
	EchoTest_t3020015217_StaticFields::get_offset_of_MAXLINES_8(),
	EchoTest_t3020015217::get_offset_of_map_9(),
	EchoTest_t3020015217::get_offset_of_facesJSON_10(),
	EchoTest_t3020015217::get_offset_of_packets_11(),
	EchoTest_t3020015217::get_offset_of_t_12(),
	EchoTest_t3020015217::get_offset_of_ids_13(),
	EchoTest_t3020015217::get_offset_of_index_14(),
	EchoTest_t3020015217::get_offset_of_dx_15(),
	EchoTest_t3020015217::get_offset_of_dy_16(),
	EchoTest_t3020015217::get_offset_of_w_17(),
	EchoTest_t3020015217::get_offset_of_simulateServer_18(),
	EchoTest_t3020015217::get_offset_of_numthreads_19(),
	EchoTest_t3020015217::get_offset_of_ticks_20(),
	EchoTest_t3020015217::get_offset_of_currentFaceNum_21(),
	EchoTest_t3020015217::get_offset_of_bWSConnected_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (U3ConWSConnectedU3Ec__Iterator0_t570499943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1645[4] = 
{
	U3ConWSConnectedU3Ec__Iterator0_t570499943::get_offset_of_U24this_0(),
	U3ConWSConnectedU3Ec__Iterator0_t570499943::get_offset_of_U24current_1(),
	U3ConWSConnectedU3Ec__Iterator0_t570499943::get_offset_of_U24disposing_2(),
	U3ConWSConnectedU3Ec__Iterator0_t570499943::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1646[6] = 
{
	U3CFetchFacesJsonU3Ec__Iterator1_t4119425628::get_offset_of_url_0(),
	U3CFetchFacesJsonU3Ec__Iterator1_t4119425628::get_offset_of_U3CwwwU3E__0_1(),
	U3CFetchFacesJsonU3Ec__Iterator1_t4119425628::get_offset_of_U24this_2(),
	U3CFetchFacesJsonU3Ec__Iterator1_t4119425628::get_offset_of_U24current_3(),
	U3CFetchFacesJsonU3Ec__Iterator1_t4119425628::get_offset_of_U24disposing_4(),
	U3CFetchFacesJsonU3Ec__Iterator1_t4119425628::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1647[6] = 
{
	U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072::get_offset_of_url_0(),
	U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072::get_offset_of_U3CwwwU3E__0_1(),
	U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072::get_offset_of_U24this_2(),
	U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072::get_offset_of_U24current_3(),
	U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072::get_offset_of_U24disposing_4(),
	U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (U3CFetchImageU3Ec__Iterator3_t3816779753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1648[7] = 
{
	U3CFetchImageU3Ec__Iterator3_t3816779753::get_offset_of_url_0(),
	U3CFetchImageU3Ec__Iterator3_t3816779753::get_offset_of_U3CwwwU3E__0_1(),
	U3CFetchImageU3Ec__Iterator3_t3816779753::get_offset_of_index_2(),
	U3CFetchImageU3Ec__Iterator3_t3816779753::get_offset_of_U24this_3(),
	U3CFetchImageU3Ec__Iterator3_t3816779753::get_offset_of_U24current_4(),
	U3CFetchImageU3Ec__Iterator3_t3816779753::get_offset_of_U24disposing_5(),
	U3CFetchImageU3Ec__Iterator3_t3816779753::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (testWebsock_t1614810498), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
