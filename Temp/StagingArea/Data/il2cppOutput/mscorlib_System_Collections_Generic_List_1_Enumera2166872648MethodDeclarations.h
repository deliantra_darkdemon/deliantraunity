﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<MapCell>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2473255285(__this, ___l0, method) ((  void (*) (Enumerator_t2166872648 *, List_1_t2632142974 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MapCell>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m788069701(__this, method) ((  void (*) (Enumerator_t2166872648 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<MapCell>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2615392077(__this, method) ((  Il2CppObject * (*) (Enumerator_t2166872648 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MapCell>::Dispose()
#define Enumerator_Dispose_m2193888690(__this, method) ((  void (*) (Enumerator_t2166872648 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<MapCell>::VerifyState()
#define Enumerator_VerifyState_m2634591003(__this, method) ((  void (*) (Enumerator_t2166872648 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<MapCell>::MoveNext()
#define Enumerator_MoveNext_m1124349501(__this, method) ((  bool (*) (Enumerator_t2166872648 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<MapCell>::get_Current()
#define Enumerator_get_Current_m745793512(__this, method) ((  MapCell_t3263021842 * (*) (Enumerator_t2166872648 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
