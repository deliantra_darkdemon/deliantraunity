﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CMap
struct CMap_t632723465;
// MapCell
struct MapCell_t3263021842;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void CMap::.ctor(System.Int32,System.Int32)
extern "C"  void CMap__ctor_m554309044 (CMap_t632723465 * __this, int32_t ___w0, int32_t ___h1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CMap::get_width()
extern "C"  int32_t CMap_get_width_m2154012433 (CMap_t632723465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CMap::get_height()
extern "C"  int32_t CMap_get_height_m661569650 (CMap_t632723465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CMap::addRow(System.Int32)
extern "C"  void CMap_addRow_m3114465430 (CMap_t632723465 * __this, int32_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MapCell CMap::getCell(System.Int32,System.Int32)
extern "C"  MapCell_t3263021842 * CMap_getCell_m3922102949 (CMap_t632723465 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CMap::serialize()
extern "C"  String_t* CMap_serialize_m1847866475 (CMap_t632723465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
