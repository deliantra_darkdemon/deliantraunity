﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EchoTest/<onWSConnected>c__Iterator0
struct U3ConWSConnectedU3Ec__Iterator0_t570499943;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EchoTest/<onWSConnected>c__Iterator0::.ctor()
extern "C"  void U3ConWSConnectedU3Ec__Iterator0__ctor_m1156059884 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EchoTest/<onWSConnected>c__Iterator0::MoveNext()
extern "C"  bool U3ConWSConnectedU3Ec__Iterator0_MoveNext_m1025350420 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EchoTest/<onWSConnected>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3ConWSConnectedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m402253988 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EchoTest/<onWSConnected>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3ConWSConnectedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m657078236 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest/<onWSConnected>c__Iterator0::Dispose()
extern "C"  void U3ConWSConnectedU3Ec__Iterator0_Dispose_m3882673845 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest/<onWSConnected>c__Iterator0::Reset()
extern "C"  void U3ConWSConnectedU3Ec__Iterator0_Reset_m78835803 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
