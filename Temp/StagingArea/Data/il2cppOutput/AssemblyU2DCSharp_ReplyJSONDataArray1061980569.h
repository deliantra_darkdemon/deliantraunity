﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t1642385972;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplyJSONDataArray
struct  ReplyJSONDataArray_t1061980569  : public Il2CppObject
{
public:
	// System.String[] ReplyJSONDataArray::data
	StringU5BU5D_t1642385972* ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(ReplyJSONDataArray_t1061980569, ___data_0)); }
	inline StringU5BU5D_t1642385972* get_data_0() const { return ___data_0; }
	inline StringU5BU5D_t1642385972** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(StringU5BU5D_t1642385972* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
