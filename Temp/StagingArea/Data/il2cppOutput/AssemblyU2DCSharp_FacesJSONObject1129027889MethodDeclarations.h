﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacesJSONObject
struct FacesJSONObject_t1129027889;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void FacesJSONObject::.ctor()
extern "C"  void FacesJSONObject__ctor_m2604168514 (FacesJSONObject_t1129027889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FacesJSONObject FacesJSONObject::fromJSON(System.String)
extern "C"  FacesJSONObject_t1129027889 * FacesJSONObject_fromJSON_m3341462460 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
