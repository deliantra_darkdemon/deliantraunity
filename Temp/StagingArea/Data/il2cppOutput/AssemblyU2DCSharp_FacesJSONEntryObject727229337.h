﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacesJSONEntryObject
struct  FacesJSONEntryObject_t727229337  : public Il2CppObject
{
public:
	// System.Int32 FacesJSONEntryObject::id
	int32_t ___id_0;
	// System.String FacesJSONEntryObject::csum
	String_t* ___csum_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(FacesJSONEntryObject_t727229337, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_csum_1() { return static_cast<int32_t>(offsetof(FacesJSONEntryObject_t727229337, ___csum_1)); }
	inline String_t* get_csum_1() const { return ___csum_1; }
	inline String_t** get_address_of_csum_1() { return &___csum_1; }
	inline void set_csum_1(String_t* value)
	{
		___csum_1 = value;
		Il2CppCodeGenWriteBarrier(&___csum_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
