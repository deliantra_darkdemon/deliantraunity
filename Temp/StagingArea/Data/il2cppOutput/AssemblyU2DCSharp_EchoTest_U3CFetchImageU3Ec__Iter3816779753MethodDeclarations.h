﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EchoTest/<FetchImage>c__Iterator3
struct U3CFetchImageU3Ec__Iterator3_t3816779753;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EchoTest/<FetchImage>c__Iterator3::.ctor()
extern "C"  void U3CFetchImageU3Ec__Iterator3__ctor_m3868575392 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EchoTest/<FetchImage>c__Iterator3::MoveNext()
extern "C"  bool U3CFetchImageU3Ec__Iterator3_MoveNext_m3570818484 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EchoTest/<FetchImage>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchImageU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1489611034 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EchoTest/<FetchImage>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchImageU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m478534082 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest/<FetchImage>c__Iterator3::Dispose()
extern "C"  void U3CFetchImageU3Ec__Iterator3_Dispose_m1978909059 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest/<FetchImage>c__Iterator3::Reset()
extern "C"  void U3CFetchImageU3Ec__Iterator3_Reset_m1839717077 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
