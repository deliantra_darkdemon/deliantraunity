﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapCellData
struct  MapCellData_t826838626  : public Il2CppObject
{
public:
	// System.Int32 MapCellData::darkness
	int32_t ___darkness_0;
	// System.Int32 MapCellData::flags
	int32_t ___flags_1;
	// System.Int32 MapCellData::player
	int32_t ___player_2;
	// System.Int32[] MapCellData::tiles
	Int32U5BU5D_t3030399641* ___tiles_3;
	// System.Int32 MapCellData::tilesCount
	int32_t ___tilesCount_4;
	// System.Int32 MapCellData::stat_width
	int32_t ___stat_width_5;
	// System.Int32 MapCellData::stat_hp
	int32_t ___stat_hp_6;
	// System.Boolean MapCellData::isDirty
	bool ___isDirty_7;

public:
	inline static int32_t get_offset_of_darkness_0() { return static_cast<int32_t>(offsetof(MapCellData_t826838626, ___darkness_0)); }
	inline int32_t get_darkness_0() const { return ___darkness_0; }
	inline int32_t* get_address_of_darkness_0() { return &___darkness_0; }
	inline void set_darkness_0(int32_t value)
	{
		___darkness_0 = value;
	}

	inline static int32_t get_offset_of_flags_1() { return static_cast<int32_t>(offsetof(MapCellData_t826838626, ___flags_1)); }
	inline int32_t get_flags_1() const { return ___flags_1; }
	inline int32_t* get_address_of_flags_1() { return &___flags_1; }
	inline void set_flags_1(int32_t value)
	{
		___flags_1 = value;
	}

	inline static int32_t get_offset_of_player_2() { return static_cast<int32_t>(offsetof(MapCellData_t826838626, ___player_2)); }
	inline int32_t get_player_2() const { return ___player_2; }
	inline int32_t* get_address_of_player_2() { return &___player_2; }
	inline void set_player_2(int32_t value)
	{
		___player_2 = value;
	}

	inline static int32_t get_offset_of_tiles_3() { return static_cast<int32_t>(offsetof(MapCellData_t826838626, ___tiles_3)); }
	inline Int32U5BU5D_t3030399641* get_tiles_3() const { return ___tiles_3; }
	inline Int32U5BU5D_t3030399641** get_address_of_tiles_3() { return &___tiles_3; }
	inline void set_tiles_3(Int32U5BU5D_t3030399641* value)
	{
		___tiles_3 = value;
		Il2CppCodeGenWriteBarrier(&___tiles_3, value);
	}

	inline static int32_t get_offset_of_tilesCount_4() { return static_cast<int32_t>(offsetof(MapCellData_t826838626, ___tilesCount_4)); }
	inline int32_t get_tilesCount_4() const { return ___tilesCount_4; }
	inline int32_t* get_address_of_tilesCount_4() { return &___tilesCount_4; }
	inline void set_tilesCount_4(int32_t value)
	{
		___tilesCount_4 = value;
	}

	inline static int32_t get_offset_of_stat_width_5() { return static_cast<int32_t>(offsetof(MapCellData_t826838626, ___stat_width_5)); }
	inline int32_t get_stat_width_5() const { return ___stat_width_5; }
	inline int32_t* get_address_of_stat_width_5() { return &___stat_width_5; }
	inline void set_stat_width_5(int32_t value)
	{
		___stat_width_5 = value;
	}

	inline static int32_t get_offset_of_stat_hp_6() { return static_cast<int32_t>(offsetof(MapCellData_t826838626, ___stat_hp_6)); }
	inline int32_t get_stat_hp_6() const { return ___stat_hp_6; }
	inline int32_t* get_address_of_stat_hp_6() { return &___stat_hp_6; }
	inline void set_stat_hp_6(int32_t value)
	{
		___stat_hp_6 = value;
	}

	inline static int32_t get_offset_of_isDirty_7() { return static_cast<int32_t>(offsetof(MapCellData_t826838626, ___isDirty_7)); }
	inline bool get_isDirty_7() const { return ___isDirty_7; }
	inline bool* get_address_of_isDirty_7() { return &___isDirty_7; }
	inline void set_isDirty_7(bool value)
	{
		___isDirty_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
