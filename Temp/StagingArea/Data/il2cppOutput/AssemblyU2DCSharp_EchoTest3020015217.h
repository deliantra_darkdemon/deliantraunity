﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// CMap
struct CMap_t632723465;
// FacesJSONObject
struct FacesJSONObject_t1129027889;
// ReplyJSONDataArray
struct ReplyJSONDataArray_t1061980569;
// System.Collections.Generic.List`1<UnityEngine.Texture>
struct List_1_t1612747451;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// WebSocket
struct WebSocket_t1213274227;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EchoTest
struct  EchoTest_t3020015217  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text EchoTest::myTextField
	Text_t356221433 * ___myTextField_2;
	// UnityEngine.GameObject EchoTest::emptySprite
	GameObject_t1756533147 * ___emptySprite_3;
	// System.String EchoTest::baseURL
	String_t* ___baseURL_4;
	// UnityEngine.Vector3 EchoTest::offset
	Vector3_t2243707580  ___offset_5;
	// System.Int32 EchoTest::extiNextId
	int32_t ___extiNextId_6;
	// System.Collections.Generic.List`1<System.String> EchoTest::lines
	List_1_t1398341365 * ___lines_7;
	// CMap EchoTest::map
	CMap_t632723465 * ___map_9;
	// FacesJSONObject EchoTest::facesJSON
	FacesJSONObject_t1129027889 * ___facesJSON_10;
	// ReplyJSONDataArray EchoTest::packets
	ReplyJSONDataArray_t1061980569 * ___packets_11;
	// System.Collections.Generic.List`1<UnityEngine.Texture> EchoTest::t
	List_1_t1612747451 * ___t_12;
	// System.Collections.Generic.List`1<System.Int32> EchoTest::ids
	List_1_t1440998580 * ___ids_13;
	// System.Int32 EchoTest::index
	int32_t ___index_14;
	// System.Int32 EchoTest::dx
	int32_t ___dx_15;
	// System.Int32 EchoTest::dy
	int32_t ___dy_16;
	// WebSocket EchoTest::w
	WebSocket_t1213274227 * ___w_17;
	// System.Boolean EchoTest::simulateServer
	bool ___simulateServer_18;
	// System.Int32 EchoTest::numthreads
	int32_t ___numthreads_19;
	// System.Int32 EchoTest::ticks
	int32_t ___ticks_20;
	// System.Int32 EchoTest::currentFaceNum
	int32_t ___currentFaceNum_21;
	// System.Boolean EchoTest::bWSConnected
	bool ___bWSConnected_22;

public:
	inline static int32_t get_offset_of_myTextField_2() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___myTextField_2)); }
	inline Text_t356221433 * get_myTextField_2() const { return ___myTextField_2; }
	inline Text_t356221433 ** get_address_of_myTextField_2() { return &___myTextField_2; }
	inline void set_myTextField_2(Text_t356221433 * value)
	{
		___myTextField_2 = value;
		Il2CppCodeGenWriteBarrier(&___myTextField_2, value);
	}

	inline static int32_t get_offset_of_emptySprite_3() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___emptySprite_3)); }
	inline GameObject_t1756533147 * get_emptySprite_3() const { return ___emptySprite_3; }
	inline GameObject_t1756533147 ** get_address_of_emptySprite_3() { return &___emptySprite_3; }
	inline void set_emptySprite_3(GameObject_t1756533147 * value)
	{
		___emptySprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___emptySprite_3, value);
	}

	inline static int32_t get_offset_of_baseURL_4() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___baseURL_4)); }
	inline String_t* get_baseURL_4() const { return ___baseURL_4; }
	inline String_t** get_address_of_baseURL_4() { return &___baseURL_4; }
	inline void set_baseURL_4(String_t* value)
	{
		___baseURL_4 = value;
		Il2CppCodeGenWriteBarrier(&___baseURL_4, value);
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___offset_5)); }
	inline Vector3_t2243707580  get_offset_5() const { return ___offset_5; }
	inline Vector3_t2243707580 * get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(Vector3_t2243707580  value)
	{
		___offset_5 = value;
	}

	inline static int32_t get_offset_of_extiNextId_6() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___extiNextId_6)); }
	inline int32_t get_extiNextId_6() const { return ___extiNextId_6; }
	inline int32_t* get_address_of_extiNextId_6() { return &___extiNextId_6; }
	inline void set_extiNextId_6(int32_t value)
	{
		___extiNextId_6 = value;
	}

	inline static int32_t get_offset_of_lines_7() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___lines_7)); }
	inline List_1_t1398341365 * get_lines_7() const { return ___lines_7; }
	inline List_1_t1398341365 ** get_address_of_lines_7() { return &___lines_7; }
	inline void set_lines_7(List_1_t1398341365 * value)
	{
		___lines_7 = value;
		Il2CppCodeGenWriteBarrier(&___lines_7, value);
	}

	inline static int32_t get_offset_of_map_9() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___map_9)); }
	inline CMap_t632723465 * get_map_9() const { return ___map_9; }
	inline CMap_t632723465 ** get_address_of_map_9() { return &___map_9; }
	inline void set_map_9(CMap_t632723465 * value)
	{
		___map_9 = value;
		Il2CppCodeGenWriteBarrier(&___map_9, value);
	}

	inline static int32_t get_offset_of_facesJSON_10() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___facesJSON_10)); }
	inline FacesJSONObject_t1129027889 * get_facesJSON_10() const { return ___facesJSON_10; }
	inline FacesJSONObject_t1129027889 ** get_address_of_facesJSON_10() { return &___facesJSON_10; }
	inline void set_facesJSON_10(FacesJSONObject_t1129027889 * value)
	{
		___facesJSON_10 = value;
		Il2CppCodeGenWriteBarrier(&___facesJSON_10, value);
	}

	inline static int32_t get_offset_of_packets_11() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___packets_11)); }
	inline ReplyJSONDataArray_t1061980569 * get_packets_11() const { return ___packets_11; }
	inline ReplyJSONDataArray_t1061980569 ** get_address_of_packets_11() { return &___packets_11; }
	inline void set_packets_11(ReplyJSONDataArray_t1061980569 * value)
	{
		___packets_11 = value;
		Il2CppCodeGenWriteBarrier(&___packets_11, value);
	}

	inline static int32_t get_offset_of_t_12() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___t_12)); }
	inline List_1_t1612747451 * get_t_12() const { return ___t_12; }
	inline List_1_t1612747451 ** get_address_of_t_12() { return &___t_12; }
	inline void set_t_12(List_1_t1612747451 * value)
	{
		___t_12 = value;
		Il2CppCodeGenWriteBarrier(&___t_12, value);
	}

	inline static int32_t get_offset_of_ids_13() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___ids_13)); }
	inline List_1_t1440998580 * get_ids_13() const { return ___ids_13; }
	inline List_1_t1440998580 ** get_address_of_ids_13() { return &___ids_13; }
	inline void set_ids_13(List_1_t1440998580 * value)
	{
		___ids_13 = value;
		Il2CppCodeGenWriteBarrier(&___ids_13, value);
	}

	inline static int32_t get_offset_of_index_14() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___index_14)); }
	inline int32_t get_index_14() const { return ___index_14; }
	inline int32_t* get_address_of_index_14() { return &___index_14; }
	inline void set_index_14(int32_t value)
	{
		___index_14 = value;
	}

	inline static int32_t get_offset_of_dx_15() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___dx_15)); }
	inline int32_t get_dx_15() const { return ___dx_15; }
	inline int32_t* get_address_of_dx_15() { return &___dx_15; }
	inline void set_dx_15(int32_t value)
	{
		___dx_15 = value;
	}

	inline static int32_t get_offset_of_dy_16() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___dy_16)); }
	inline int32_t get_dy_16() const { return ___dy_16; }
	inline int32_t* get_address_of_dy_16() { return &___dy_16; }
	inline void set_dy_16(int32_t value)
	{
		___dy_16 = value;
	}

	inline static int32_t get_offset_of_w_17() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___w_17)); }
	inline WebSocket_t1213274227 * get_w_17() const { return ___w_17; }
	inline WebSocket_t1213274227 ** get_address_of_w_17() { return &___w_17; }
	inline void set_w_17(WebSocket_t1213274227 * value)
	{
		___w_17 = value;
		Il2CppCodeGenWriteBarrier(&___w_17, value);
	}

	inline static int32_t get_offset_of_simulateServer_18() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___simulateServer_18)); }
	inline bool get_simulateServer_18() const { return ___simulateServer_18; }
	inline bool* get_address_of_simulateServer_18() { return &___simulateServer_18; }
	inline void set_simulateServer_18(bool value)
	{
		___simulateServer_18 = value;
	}

	inline static int32_t get_offset_of_numthreads_19() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___numthreads_19)); }
	inline int32_t get_numthreads_19() const { return ___numthreads_19; }
	inline int32_t* get_address_of_numthreads_19() { return &___numthreads_19; }
	inline void set_numthreads_19(int32_t value)
	{
		___numthreads_19 = value;
	}

	inline static int32_t get_offset_of_ticks_20() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___ticks_20)); }
	inline int32_t get_ticks_20() const { return ___ticks_20; }
	inline int32_t* get_address_of_ticks_20() { return &___ticks_20; }
	inline void set_ticks_20(int32_t value)
	{
		___ticks_20 = value;
	}

	inline static int32_t get_offset_of_currentFaceNum_21() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___currentFaceNum_21)); }
	inline int32_t get_currentFaceNum_21() const { return ___currentFaceNum_21; }
	inline int32_t* get_address_of_currentFaceNum_21() { return &___currentFaceNum_21; }
	inline void set_currentFaceNum_21(int32_t value)
	{
		___currentFaceNum_21 = value;
	}

	inline static int32_t get_offset_of_bWSConnected_22() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217, ___bWSConnected_22)); }
	inline bool get_bWSConnected_22() const { return ___bWSConnected_22; }
	inline bool* get_address_of_bWSConnected_22() { return &___bWSConnected_22; }
	inline void set_bWSConnected_22(bool value)
	{
		___bWSConnected_22 = value;
	}
};

struct EchoTest_t3020015217_StaticFields
{
public:
	// System.Int32 EchoTest::MAXLINES
	int32_t ___MAXLINES_8;

public:
	inline static int32_t get_offset_of_MAXLINES_8() { return static_cast<int32_t>(offsetof(EchoTest_t3020015217_StaticFields, ___MAXLINES_8)); }
	inline int32_t get_MAXLINES_8() const { return ___MAXLINES_8; }
	inline int32_t* get_address_of_MAXLINES_8() { return &___MAXLINES_8; }
	inline void set_MAXLINES_8(int32_t value)
	{
		___MAXLINES_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
