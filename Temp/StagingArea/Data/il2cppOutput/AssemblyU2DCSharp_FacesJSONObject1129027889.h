﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FacesJSONEntryObject[]
struct FacesJSONEntryObjectU5BU5D_t2881799652;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacesJSONObject
struct  FacesJSONObject_t1129027889  : public Il2CppObject
{
public:
	// FacesJSONEntryObject[] FacesJSONObject::data
	FacesJSONEntryObjectU5BU5D_t2881799652* ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(FacesJSONObject_t1129027889, ___data_0)); }
	inline FacesJSONEntryObjectU5BU5D_t2881799652* get_data_0() const { return ___data_0; }
	inline FacesJSONEntryObjectU5BU5D_t2881799652** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(FacesJSONEntryObjectU5BU5D_t2881799652* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
