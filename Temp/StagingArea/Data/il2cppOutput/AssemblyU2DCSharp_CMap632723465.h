﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<MapRow>
struct List_1_t3801961778;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CMap
struct  CMap_t632723465  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<MapRow> CMap::rows
	List_1_t3801961778 * ___rows_0;

public:
	inline static int32_t get_offset_of_rows_0() { return static_cast<int32_t>(offsetof(CMap_t632723465, ___rows_0)); }
	inline List_1_t3801961778 * get_rows_0() const { return ___rows_0; }
	inline List_1_t3801961778 ** get_address_of_rows_0() { return &___rows_0; }
	inline void set_rows_0(List_1_t3801961778 * value)
	{
		___rows_0 = value;
		Il2CppCodeGenWriteBarrier(&___rows_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
