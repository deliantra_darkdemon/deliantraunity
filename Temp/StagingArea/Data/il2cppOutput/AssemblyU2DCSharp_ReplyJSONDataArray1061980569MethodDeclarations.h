﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplyJSONDataArray
struct ReplyJSONDataArray_t1061980569;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ReplyJSONDataArray::.ctor()
extern "C"  void ReplyJSONDataArray__ctor_m3167406150 (ReplyJSONDataArray_t1061980569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplyJSONDataArray ReplyJSONDataArray::fromDeliantraJSON(System.String)
extern "C"  ReplyJSONDataArray_t1061980569 * ReplyJSONDataArray_fromDeliantraJSON_m506824200 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplyJSONDataArray ReplyJSONDataArray::fromJSON(System.String)
extern "C"  ReplyJSONDataArray_t1061980569 * ReplyJSONDataArray_fromJSON_m1557679406 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
