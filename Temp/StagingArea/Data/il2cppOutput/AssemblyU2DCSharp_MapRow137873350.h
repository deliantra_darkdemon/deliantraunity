﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<MapCell>
struct List_1_t2632142974;
// MapCell
struct MapCell_t3263021842;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapRow
struct  MapRow_t137873350  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<MapCell> MapRow::cells
	List_1_t2632142974 * ___cells_0;
	// MapCell MapRow::firstCell
	MapCell_t3263021842 * ___firstCell_1;
	// MapCell MapRow::lastCell
	MapCell_t3263021842 * ___lastCell_2;

public:
	inline static int32_t get_offset_of_cells_0() { return static_cast<int32_t>(offsetof(MapRow_t137873350, ___cells_0)); }
	inline List_1_t2632142974 * get_cells_0() const { return ___cells_0; }
	inline List_1_t2632142974 ** get_address_of_cells_0() { return &___cells_0; }
	inline void set_cells_0(List_1_t2632142974 * value)
	{
		___cells_0 = value;
		Il2CppCodeGenWriteBarrier(&___cells_0, value);
	}

	inline static int32_t get_offset_of_firstCell_1() { return static_cast<int32_t>(offsetof(MapRow_t137873350, ___firstCell_1)); }
	inline MapCell_t3263021842 * get_firstCell_1() const { return ___firstCell_1; }
	inline MapCell_t3263021842 ** get_address_of_firstCell_1() { return &___firstCell_1; }
	inline void set_firstCell_1(MapCell_t3263021842 * value)
	{
		___firstCell_1 = value;
		Il2CppCodeGenWriteBarrier(&___firstCell_1, value);
	}

	inline static int32_t get_offset_of_lastCell_2() { return static_cast<int32_t>(offsetof(MapRow_t137873350, ___lastCell_2)); }
	inline MapCell_t3263021842 * get_lastCell_2() const { return ___lastCell_2; }
	inline MapCell_t3263021842 ** get_address_of_lastCell_2() { return &___lastCell_2; }
	inline void set_lastCell_2(MapCell_t3263021842 * value)
	{
		___lastCell_2 = value;
		Il2CppCodeGenWriteBarrier(&___lastCell_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
