﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// testWebsock
struct testWebsock_t1614810498;

#include "codegen/il2cpp-codegen.h"

// System.Void testWebsock::.ctor()
extern "C"  void testWebsock__ctor_m3225857622 (testWebsock_t1614810498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testWebsock::Main()
extern "C"  void testWebsock_Main_m2822579711 (testWebsock_t1614810498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
