﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapCell
struct MapCell_t3263021842;

#include "codegen/il2cpp-codegen.h"

// System.Void MapCell::.ctor()
extern "C"  void MapCell__ctor_m1307241529 (MapCell_t3263021842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
