﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EchoTest
struct EchoTest_t3020015217;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// MapCellData
struct MapCellData_t826838626;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_MapCellData826838626.h"

// System.Void EchoTest::.ctor()
extern "C"  void EchoTest__ctor_m2064569680 (EchoTest_t3020015217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::Start()
extern "C"  void EchoTest_Start_m2090771824 (EchoTest_t3020015217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EchoTest::onWSConnected()
extern "C"  Il2CppObject * EchoTest_onWSConnected_m1647819914 (EchoTest_t3020015217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EchoTest::FetchFacesJson(System.String)
extern "C"  Il2CppObject * EchoTest_FetchFacesJson_m1267621756 (EchoTest_t3020015217 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EchoTest::FetchPacketsJson(System.String)
extern "C"  Il2CppObject * EchoTest_FetchPacketsJson_m4197616203 (EchoTest_t3020015217 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::onPacketsJSONLoaded(System.String)
extern "C"  void EchoTest_onPacketsJSONLoaded_m216094539 (EchoTest_t3020015217 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::onFacesJSONLoaded(System.String)
extern "C"  void EchoTest_onFacesJSONLoaded_m1919704972 (EchoTest_t3020015217 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EchoTest::FetchImage(System.String,System.Int32)
extern "C"  Il2CppObject * EchoTest_FetchImage_m1536496846 (EchoTest_t3020015217 * __this, String_t* ___url0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::OnGUI()
extern "C"  void EchoTest_OnGUI_m234373644 (EchoTest_t3020015217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EchoTest::getTextureIndex(System.Int32)
extern "C"  int32_t EchoTest_getTextureIndex_m624472290 (EchoTest_t3020015217 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EchoTest::findIndex(MapCellData,System.Int32)
extern "C"  int32_t EchoTest_findIndex_m3032432620 (EchoTest_t3020015217 * __this, MapCellData_t826838626 * ___data0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::onConnect()
extern "C"  void EchoTest_onConnect_m417626763 (EchoTest_t3020015217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::send(System.String)
extern "C"  void EchoTest_send_m1990524934 (EchoTest_t3020015217 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::onRawMessage(System.Byte[])
extern "C"  void EchoTest_onRawMessage_m309866093 (EchoTest_t3020015217 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::onMessage(System.String)
extern "C"  void EchoTest_onMessage_m3738535102 (EchoTest_t3020015217 * __this, String_t* ___encodedMessage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::scroll_map()
extern "C"  void EchoTest_scroll_map_m2763410202 (EchoTest_t3020015217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::need_facenum(System.Int32)
extern "C"  void EchoTest_need_facenum_m3893050015 (EchoTest_t3020015217 * __this, int32_t ___face0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::Update()
extern "C"  void EchoTest_Update_m3540047033 (EchoTest_t3020015217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EchoTest::findCsum(System.Int32)
extern "C"  String_t* EchoTest_findCsum_m2560875761 (EchoTest_t3020015217 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest::.cctor()
extern "C"  void EchoTest__cctor_m2199241283 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
