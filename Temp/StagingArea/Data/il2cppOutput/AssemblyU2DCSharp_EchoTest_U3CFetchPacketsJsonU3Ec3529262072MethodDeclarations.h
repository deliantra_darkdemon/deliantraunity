﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EchoTest/<FetchPacketsJson>c__Iterator2
struct U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EchoTest/<FetchPacketsJson>c__Iterator2::.ctor()
extern "C"  void U3CFetchPacketsJsonU3Ec__Iterator2__ctor_m181738545 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EchoTest/<FetchPacketsJson>c__Iterator2::MoveNext()
extern "C"  bool U3CFetchPacketsJsonU3Ec__Iterator2_MoveNext_m1219194327 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EchoTest/<FetchPacketsJson>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchPacketsJsonU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3990172979 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EchoTest/<FetchPacketsJson>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchPacketsJsonU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m700220187 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest/<FetchPacketsJson>c__Iterator2::Dispose()
extern "C"  void U3CFetchPacketsJsonU3Ec__Iterator2_Dispose_m1200506034 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EchoTest/<FetchPacketsJson>c__Iterator2::Reset()
extern "C"  void U3CFetchPacketsJsonU3Ec__Iterator2_Reset_m3434169572 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
