﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// EchoTest
struct EchoTest_t3020015217;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EchoTest/<FetchPacketsJson>c__Iterator2
struct  U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072  : public Il2CppObject
{
public:
	// System.String EchoTest/<FetchPacketsJson>c__Iterator2::url
	String_t* ___url_0;
	// UnityEngine.WWW EchoTest/<FetchPacketsJson>c__Iterator2::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_1;
	// EchoTest EchoTest/<FetchPacketsJson>c__Iterator2::$this
	EchoTest_t3020015217 * ___U24this_2;
	// System.Object EchoTest/<FetchPacketsJson>c__Iterator2::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean EchoTest/<FetchPacketsJson>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 EchoTest/<FetchPacketsJson>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072, ___U3CwwwU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072, ___U24this_2)); }
	inline EchoTest_t3020015217 * get_U24this_2() const { return ___U24this_2; }
	inline EchoTest_t3020015217 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(EchoTest_t3020015217 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
