﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MapCellData
struct MapCellData_t826838626;

#include "codegen/il2cpp-codegen.h"

// System.Void MapCellData::.ctor()
extern "C"  void MapCellData__ctor_m2952753737 (MapCellData_t826838626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MapCellData::clear()
extern "C"  void MapCellData_clear_m1366406508 (MapCellData_t826838626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
