﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MapCellData
struct MapCellData_t826838626;
// MapCell
struct MapCell_t3263021842;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapCell
struct  MapCell_t3263021842  : public Il2CppObject
{
public:
	// MapCellData MapCell::data
	MapCellData_t826838626 * ___data_0;
	// MapCell MapCell::previous
	MapCell_t3263021842 * ___previous_1;
	// MapCell MapCell::next
	MapCell_t3263021842 * ___next_2;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(MapCell_t3263021842, ___data_0)); }
	inline MapCellData_t826838626 * get_data_0() const { return ___data_0; }
	inline MapCellData_t826838626 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(MapCellData_t826838626 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier(&___data_0, value);
	}

	inline static int32_t get_offset_of_previous_1() { return static_cast<int32_t>(offsetof(MapCell_t3263021842, ___previous_1)); }
	inline MapCell_t3263021842 * get_previous_1() const { return ___previous_1; }
	inline MapCell_t3263021842 ** get_address_of_previous_1() { return &___previous_1; }
	inline void set_previous_1(MapCell_t3263021842 * value)
	{
		___previous_1 = value;
		Il2CppCodeGenWriteBarrier(&___previous_1, value);
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(MapCell_t3263021842, ___next_2)); }
	inline MapCell_t3263021842 * get_next_2() const { return ___next_2; }
	inline MapCell_t3263021842 ** get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(MapCell_t3263021842 * value)
	{
		___next_2 = value;
		Il2CppCodeGenWriteBarrier(&___next_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
