﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// CameraController
struct CameraController_t3555666667;
// CMap
struct CMap_t632723465;
// MapCell
struct MapCell_t3263021842;
// System.String
struct String_t;
// EchoTest
struct EchoTest_t3020015217;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// MapCellData
struct MapCellData_t826838626;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// EchoTest/<FetchFacesJson>c__Iterator1
struct U3CFetchFacesJsonU3Ec__Iterator1_t4119425628;
// System.Object
struct Il2CppObject;
// EchoTest/<FetchImage>c__Iterator3
struct U3CFetchImageU3Ec__Iterator3_t3816779753;
// EchoTest/<FetchPacketsJson>c__Iterator2
struct U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072;
// EchoTest/<onWSConnected>c__Iterator0
struct U3ConWSConnectedU3Ec__Iterator0_t570499943;
// FacesJSONEntryObject
struct FacesJSONEntryObject_t727229337;
// FacesJSONObject
struct FacesJSONObject_t1129027889;
// MapRow
struct MapRow_t137873350;
// ReplyJSONDataArray
struct ReplyJSONDataArray_t1061980569;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_CameraController3555666667.h"
#include "AssemblyU2DCSharp_CameraController3555666667MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "AssemblyU2DCSharp_CMap632723465.h"
#include "AssemblyU2DCSharp_CMap632723465MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3801961778MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3801961778.h"
#include "AssemblyU2DCSharp_MapRow137873350MethodDeclarations.h"
#include "AssemblyU2DCSharp_MapRow137873350.h"
#include "AssemblyU2DCSharp_MapCell3263021842.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2632142974MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2632142974.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AssemblyU2DCSharp_MapCellData826838626.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_EchoTest3020015217.h"
#include "AssemblyU2DCSharp_EchoTest3020015217MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612747451MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612747451.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "System_System_UriBuilder2016461725MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_WebSocket1213274227MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "System_System_UriBuilder2016461725.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "System_System_Uri19570940.h"
#include "AssemblyU2DCSharpU2Dfirstpass_WebSocket1213274227.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_EchoTest_U3ConWSConnectedU3Ec__It570499943MethodDeclarations.h"
#include "AssemblyU2DCSharp_EchoTest_U3ConWSConnectedU3Ec__It570499943.h"
#include "AssemblyU2DCSharp_EchoTest_U3CFetchFacesJsonU3Ec__4119425628MethodDeclarations.h"
#include "AssemblyU2DCSharp_EchoTest_U3CFetchFacesJsonU3Ec__4119425628.h"
#include "AssemblyU2DCSharp_EchoTest_U3CFetchPacketsJsonU3Ec3529262072MethodDeclarations.h"
#include "AssemblyU2DCSharp_EchoTest_U3CFetchPacketsJsonU3Ec3529262072.h"
#include "AssemblyU2DCSharp_ReplyJSONDataArray1061980569MethodDeclarations.h"
#include "AssemblyU2DCSharp_ReplyJSONDataArray1061980569.h"
#include "AssemblyU2DCSharp_FacesJSONObject1129027889MethodDeclarations.h"
#include "AssemblyU2DCSharp_FacesJSONObject1129027889.h"
#include "AssemblyU2DCSharp_EchoTest_U3CFetchImageU3Ec__Iter3816779753MethodDeclarations.h"
#include "AssemblyU2DCSharp_EchoTest_U3CFetchImageU3Ec__Iter3816779753.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "AssemblyU2DCSharp_MapCellData826838626MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_System_Char3454481338.h"
#include "AssemblyU2DCSharp_MapCell3263021842MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_FacesJSONEntryObject727229337.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "AssemblyU2DCSharp_FacesJSONEntryObject727229337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JsonUtility653638946MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JsonUtility653638946.h"

// !!0 UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern "C"  Il2CppObject * JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define JsonUtility_FromJson_TisIl2CppObject_m3451544451(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<FacesJSONObject>(System.String)
#define JsonUtility_FromJson_TisFacesJSONObject_t1129027889_m706920862(__this /* static, unused */, p0, method) ((  FacesJSONObject_t1129027889 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<ReplyJSONDataArray>(System.String)
#define JsonUtility_FromJson_TisReplyJSONDataArray_t1061980569_m2871469018(__this /* static, unused */, p0, method) ((  ReplyJSONDataArray_t1061980569 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m3451544451_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraController::.ctor()
extern "C"  void CameraController__ctor_m3279944138 (CameraController_t3555666667 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraController::Start()
extern "C"  void CameraController_Start_m2156540118 (CameraController_t3555666667 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraController::Update()
extern "C"  void CameraController_Update_m2439802515 (CameraController_t3555666667 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CMap::.ctor(System.Int32,System.Int32)
extern Il2CppClass* List_1_t3801961778_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2226289555_MethodInfo_var;
extern const uint32_t CMap__ctor_m554309044_MetadataUsageId;
extern "C"  void CMap__ctor_m554309044 (CMap_t632723465 * __this, int32_t ___w0, int32_t ___h1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CMap__ctor_m554309044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3801961778 * L_0 = (List_1_t3801961778 *)il2cpp_codegen_object_new(List_1_t3801961778_il2cpp_TypeInfo_var);
		List_1__ctor_m2226289555(L_0, /*hidden argument*/List_1__ctor_m2226289555_MethodInfo_var);
		__this->set_rows_0(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		goto IL_001d;
	}

IL_0016:
	{
		int32_t L_1 = ___w0;
		CMap_addRow_m3114465430(__this, L_1, /*hidden argument*/NULL);
	}

IL_001d:
	{
		int32_t L_2 = CMap_get_height_m661569650(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___h1;
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_0016;
		}
	}
	{
		return;
	}
}
// System.Int32 CMap::get_width()
extern const MethodInfo* List_1_get_Count_m3386062531_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3655811018_MethodInfo_var;
extern const uint32_t CMap_get_width_m2154012433_MetadataUsageId;
extern "C"  int32_t CMap_get_width_m2154012433 (CMap_t632723465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CMap_get_width_m2154012433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3801961778 * L_0 = __this->get_rows_0();
		int32_t L_1 = List_1_get_Count_m3386062531(L_0, /*hidden argument*/List_1_get_Count_m3386062531_MethodInfo_var);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return 0;
	}

IL_0012:
	{
		List_1_t3801961778 * L_2 = __this->get_rows_0();
		MapRow_t137873350 * L_3 = List_1_get_Item_m3655811018(L_2, 0, /*hidden argument*/List_1_get_Item_m3655811018_MethodInfo_var);
		int32_t L_4 = MapRow_get_length_m3503286366(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 CMap::get_height()
extern const MethodInfo* List_1_get_Count_m3386062531_MethodInfo_var;
extern const uint32_t CMap_get_height_m661569650_MetadataUsageId;
extern "C"  int32_t CMap_get_height_m661569650 (CMap_t632723465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CMap_get_height_m661569650_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3801961778 * L_0 = __this->get_rows_0();
		int32_t L_1 = List_1_get_Count_m3386062531(L_0, /*hidden argument*/List_1_get_Count_m3386062531_MethodInfo_var);
		return L_1;
	}
}
// System.Void CMap::addRow(System.Int32)
extern Il2CppClass* MapRow_t137873350_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m1116316207_MethodInfo_var;
extern const uint32_t CMap_addRow_m3114465430_MetadataUsageId;
extern "C"  void CMap_addRow_m3114465430 (CMap_t632723465 * __this, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CMap_addRow_m3114465430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3801961778 * L_0 = __this->get_rows_0();
		int32_t L_1 = ___size0;
		MapRow_t137873350 * L_2 = (MapRow_t137873350 *)il2cpp_codegen_object_new(MapRow_t137873350_il2cpp_TypeInfo_var);
		MapRow__ctor_m3625560204(L_2, L_1, /*hidden argument*/NULL);
		List_1_Add_m1116316207(L_0, L_2, /*hidden argument*/List_1_Add_m1116316207_MethodInfo_var);
		return;
	}
}
// MapCell CMap::getCell(System.Int32,System.Int32)
extern const MethodInfo* List_1_get_Item_m3655811018_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2216661166_MethodInfo_var;
extern const uint32_t CMap_getCell_m3922102949_MetadataUsageId;
extern "C"  MapCell_t3263021842 * CMap_getCell_m3922102949 (CMap_t632723465 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CMap_getCell_m3922102949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = CMap_get_width_m2154012433(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_2 = ___y1;
		int32_t L_3 = CMap_get_height_m661569650(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_0030;
		}
	}
	{
		List_1_t3801961778 * L_4 = __this->get_rows_0();
		int32_t L_5 = ___y1;
		MapRow_t137873350 * L_6 = List_1_get_Item_m3655811018(L_4, L_5, /*hidden argument*/List_1_get_Item_m3655811018_MethodInfo_var);
		List_1_t2632142974 * L_7 = L_6->get_cells_0();
		int32_t L_8 = ___x0;
		MapCell_t3263021842 * L_9 = List_1_get_Item_m2216661166(L_7, L_8, /*hidden argument*/List_1_get_Item_m2216661166_MethodInfo_var);
		return L_9;
	}

IL_0030:
	{
		return (MapCell_t3263021842 *)NULL;
	}
}
// System.String CMap::serialize()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t CMap_serialize_m1847866475_MetadataUsageId;
extern "C"  String_t* CMap_serialize_m1847866475 (CMap_t632723465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CMap_serialize_m1847866475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	MapCell_t3263021842 * V_3 = NULL;
	String_t* V_4 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		V_1 = 0;
		goto IL_007e;
	}

IL_000d:
	{
		V_2 = 0;
		goto IL_0062;
	}

IL_0014:
	{
		int32_t L_1 = V_2;
		int32_t L_2 = V_1;
		MapCell_t3263021842 * L_3 = CMap_getCell_m3922102949(__this, L_1, L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		MapCell_t3263021842 * L_4 = V_3;
		MapCellData_t826838626 * L_5 = L_4->get_data_0();
		int32_t* L_6 = L_5->get_address_of_darkness_0();
		String_t* L_7 = Int32_ToString_m2960866144(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		goto IL_0048;
	}

IL_003a:
	{
		String_t* L_8 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029326, L_8, /*hidden argument*/NULL);
		V_4 = L_9;
	}

IL_0048:
	{
		String_t* L_10 = V_4;
		int32_t L_11 = String_get_Length_m1606060069(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)3)))
		{
			goto IL_003a;
		}
	}
	{
		String_t* L_12 = V_0;
		String_t* L_13 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2596409543(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0062:
	{
		int32_t L_16 = V_2;
		int32_t L_17 = CMap_get_width_m2154012433(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, L_18, _stringLiteral372029352, /*hidden argument*/NULL);
		V_0 = L_19;
		int32_t L_20 = V_1;
		V_1 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_007e:
	{
		int32_t L_21 = V_1;
		int32_t L_22 = CMap_get_height_m661569650(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_23 = V_0;
		return L_23;
	}
}
// System.Void EchoTest::.ctor()
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* CMap_t632723465_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1612747451_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2767293869_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3311112068_MethodInfo_var;
extern const uint32_t EchoTest__ctor_m2064569680_MetadataUsageId;
extern "C"  void EchoTest__ctor_m2064569680 (EchoTest_t3020015217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest__ctor_m2064569680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_extiNextId_6(((int32_t)100));
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_lines_7(L_0);
		CMap_t632723465 * L_1 = (CMap_t632723465 *)il2cpp_codegen_object_new(CMap_t632723465_il2cpp_TypeInfo_var);
		CMap__ctor_m554309044(L_1, ((int32_t)12), ((int32_t)12), /*hidden argument*/NULL);
		__this->set_map_9(L_1);
		List_1_t1612747451 * L_2 = (List_1_t1612747451 *)il2cpp_codegen_object_new(List_1_t1612747451_il2cpp_TypeInfo_var);
		List_1__ctor_m2767293869(L_2, /*hidden argument*/List_1__ctor_m2767293869_MethodInfo_var);
		__this->set_t_12(L_2);
		List_1_t1440998580 * L_3 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m3311112068(L_3, /*hidden argument*/List_1__ctor_m3311112068_MethodInfo_var);
		__this->set_ids_13(L_3);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EchoTest::Start()
extern Il2CppClass* UriBuilder_t2016461725_il2cpp_TypeInfo_var;
extern Il2CppClass* WebSocket_t1213274227_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2344356431;
extern Il2CppCodeGenString* _stringLiteral100571852;
extern Il2CppCodeGenString* _stringLiteral2599108301;
extern Il2CppCodeGenString* _stringLiteral729096013;
extern Il2CppCodeGenString* _stringLiteral3907649725;
extern Il2CppCodeGenString* _stringLiteral1197655679;
extern Il2CppCodeGenString* _stringLiteral3616683020;
extern Il2CppCodeGenString* _stringLiteral1798850466;
extern Il2CppCodeGenString* _stringLiteral4280192764;
extern Il2CppCodeGenString* _stringLiteral3110053184;
extern Il2CppCodeGenString* _stringLiteral1146366099;
extern Il2CppCodeGenString* _stringLiteral4178795669;
extern const uint32_t EchoTest_Start_m2090771824_MetadataUsageId;
extern "C"  void EchoTest_Start_m2090771824 (EchoTest_t3020015217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_Start_m2090771824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UriBuilder_t2016461725 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		bool L_0 = __this->get_simulateServer_18();
		if (L_0)
		{
			goto IL_0060;
		}
	}
	{
		Application_ExternalEval_m2388671608(NULL /*static, unused*/, _stringLiteral2344356431, /*hidden argument*/NULL);
		Application_ExternalEval_m2388671608(NULL /*static, unused*/, _stringLiteral100571852, /*hidden argument*/NULL);
		Application_ExternalEval_m2388671608(NULL /*static, unused*/, _stringLiteral2599108301, /*hidden argument*/NULL);
		Application_ExternalEval_m2388671608(NULL /*static, unused*/, _stringLiteral729096013, /*hidden argument*/NULL);
		Application_ExternalEval_m2388671608(NULL /*static, unused*/, _stringLiteral3907649725, /*hidden argument*/NULL);
		Application_ExternalEval_m2388671608(NULL /*static, unused*/, _stringLiteral1197655679, /*hidden argument*/NULL);
		Application_ExternalEval_m2388671608(NULL /*static, unused*/, _stringLiteral3616683020, /*hidden argument*/NULL);
		Application_ExternalEval_m2388671608(NULL /*static, unused*/, _stringLiteral1798850466, /*hidden argument*/NULL);
		goto IL_00bd;
	}

IL_0060:
	{
		UriBuilder_t2016461725 * L_1 = (UriBuilder_t2016461725 *)il2cpp_codegen_object_new(UriBuilder_t2016461725_il2cpp_TypeInfo_var);
		UriBuilder__ctor_m3402167180(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
		UriBuilder_t2016461725 * L_2 = V_0;
		UriBuilder_set_Host_m1829532633(L_2, _stringLiteral4280192764, /*hidden argument*/NULL);
		UriBuilder_t2016461725 * L_3 = V_0;
		UriBuilder_set_Scheme_m920228366(L_3, _stringLiteral3110053184, /*hidden argument*/NULL);
		UriBuilder_t2016461725 * L_4 = V_0;
		UriBuilder_set_Port_m399873979(L_4, ((int32_t)13327), /*hidden argument*/NULL);
		UriBuilder_t2016461725 * L_5 = V_0;
		UriBuilder_set_Path_m1291193322(L_5, _stringLiteral1146366099, /*hidden argument*/NULL);
		UriBuilder_t2016461725 * L_6 = V_0;
		Uri_t19570940 * L_7 = UriBuilder_get_Uri_m1365242951(L_6, /*hidden argument*/NULL);
		WebSocket_t1213274227 * L_8 = (WebSocket_t1213274227 *)il2cpp_codegen_object_new(WebSocket_t1213274227_il2cpp_TypeInfo_var);
		WebSocket__ctor_m2447929677(L_8, L_7, /*hidden argument*/NULL);
		__this->set_w_17(L_8);
		UriBuilder_t2016461725 * L_9 = V_0;
		Uri_t19570940 * L_10 = UriBuilder_get_Uri_m1365242951(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Il2CppObject * L_11 = EchoTest_onWSConnected_m1647819914(__this, /*hidden argument*/NULL);
		V_1 = L_11;
		Il2CppObject * L_12 = V_1;
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_12, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		Il2CppObject * L_13 = EchoTest_FetchFacesJson_m1267621756(__this, _stringLiteral4178795669, /*hidden argument*/NULL);
		V_2 = L_13;
		Il2CppObject * L_14 = V_2;
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator EchoTest::onWSConnected()
extern Il2CppClass* U3ConWSConnectedU3Ec__Iterator0_t570499943_il2cpp_TypeInfo_var;
extern const uint32_t EchoTest_onWSConnected_m1647819914_MetadataUsageId;
extern "C"  Il2CppObject * EchoTest_onWSConnected_m1647819914 (EchoTest_t3020015217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_onWSConnected_m1647819914_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3ConWSConnectedU3Ec__Iterator0_t570499943 * V_0 = NULL;
	{
		U3ConWSConnectedU3Ec__Iterator0_t570499943 * L_0 = (U3ConWSConnectedU3Ec__Iterator0_t570499943 *)il2cpp_codegen_object_new(U3ConWSConnectedU3Ec__Iterator0_t570499943_il2cpp_TypeInfo_var);
		U3ConWSConnectedU3Ec__Iterator0__ctor_m1156059884(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3ConWSConnectedU3Ec__Iterator0_t570499943 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3ConWSConnectedU3Ec__Iterator0_t570499943 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator EchoTest::FetchFacesJson(System.String)
extern Il2CppClass* U3CFetchFacesJsonU3Ec__Iterator1_t4119425628_il2cpp_TypeInfo_var;
extern const uint32_t EchoTest_FetchFacesJson_m1267621756_MetadataUsageId;
extern "C"  Il2CppObject * EchoTest_FetchFacesJson_m1267621756 (EchoTest_t3020015217 * __this, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_FetchFacesJson_m1267621756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * V_0 = NULL;
	{
		U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * L_0 = (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 *)il2cpp_codegen_object_new(U3CFetchFacesJsonU3Ec__Iterator1_t4119425628_il2cpp_TypeInfo_var);
		U3CFetchFacesJsonU3Ec__Iterator1__ctor_m3122957467(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * L_1 = V_0;
		String_t* L_2 = ___url0;
		L_1->set_url_0(L_2);
		U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * L_3 = V_0;
		L_3->set_U24this_2(__this);
		U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator EchoTest::FetchPacketsJson(System.String)
extern Il2CppClass* U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072_il2cpp_TypeInfo_var;
extern const uint32_t EchoTest_FetchPacketsJson_m4197616203_MetadataUsageId;
extern "C"  Il2CppObject * EchoTest_FetchPacketsJson_m4197616203 (EchoTest_t3020015217 * __this, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_FetchPacketsJson_m4197616203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * V_0 = NULL;
	{
		U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * L_0 = (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 *)il2cpp_codegen_object_new(U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072_il2cpp_TypeInfo_var);
		U3CFetchPacketsJsonU3Ec__Iterator2__ctor_m181738545(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * L_1 = V_0;
		String_t* L_2 = ___url0;
		L_1->set_url_0(L_2);
		U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * L_3 = V_0;
		L_3->set_U24this_2(__this);
		U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * L_4 = V_0;
		return L_4;
	}
}
// System.Void EchoTest::onPacketsJSONLoaded(System.String)
extern "C"  void EchoTest_onPacketsJSONLoaded_m216094539 (EchoTest_t3020015217 * __this, String_t* ___s0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___s0;
		ReplyJSONDataArray_t1061980569 * L_1 = ReplyJSONDataArray_fromJSON_m1557679406(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_packets_11(L_1);
		return;
	}
}
// System.Void EchoTest::onFacesJSONLoaded(System.String)
extern "C"  void EchoTest_onFacesJSONLoaded_m1919704972 (EchoTest_t3020015217 * __this, String_t* ___s0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___s0;
		FacesJSONObject_t1129027889 * L_1 = FacesJSONObject_fromJSON_m3341462460(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_facesJSON_10(L_1);
		return;
	}
}
// System.Collections.IEnumerator EchoTest::FetchImage(System.String,System.Int32)
extern Il2CppClass* U3CFetchImageU3Ec__Iterator3_t3816779753_il2cpp_TypeInfo_var;
extern const uint32_t EchoTest_FetchImage_m1536496846_MetadataUsageId;
extern "C"  Il2CppObject * EchoTest_FetchImage_m1536496846 (EchoTest_t3020015217 * __this, String_t* ___url0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_FetchImage_m1536496846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFetchImageU3Ec__Iterator3_t3816779753 * V_0 = NULL;
	{
		U3CFetchImageU3Ec__Iterator3_t3816779753 * L_0 = (U3CFetchImageU3Ec__Iterator3_t3816779753 *)il2cpp_codegen_object_new(U3CFetchImageU3Ec__Iterator3_t3816779753_il2cpp_TypeInfo_var);
		U3CFetchImageU3Ec__Iterator3__ctor_m3868575392(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFetchImageU3Ec__Iterator3_t3816779753 * L_1 = V_0;
		String_t* L_2 = ___url0;
		L_1->set_url_0(L_2);
		U3CFetchImageU3Ec__Iterator3_t3816779753 * L_3 = V_0;
		int32_t L_4 = ___index1;
		L_3->set_index_2(L_4);
		U3CFetchImageU3Ec__Iterator3_t3816779753 * L_5 = V_0;
		L_5->set_U24this_3(__this);
		U3CFetchImageU3Ec__Iterator3_t3816779753 * L_6 = V_0;
		return L_6;
	}
}
// System.Void EchoTest::OnGUI()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m852068579_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3764379494_MethodInfo_var;
extern const uint32_t EchoTest_OnGUI_m234373644_MetadataUsageId;
extern "C"  void EchoTest_OnGUI_m234373644 (EchoTest_t3020015217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_OnGUI_m234373644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	MapCellData_t826838626 * V_3 = NULL;
	int32_t V_4 = 0;
	{
		List_1_t1440998580 * L_0 = __this->get_ids_13();
		int32_t L_1 = List_1_get_Count_m852068579(L_0, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_00d9;
	}

IL_001b:
	{
		V_2 = 0;
		goto IL_00c4;
	}

IL_0022:
	{
		CMap_t632723465 * L_2 = __this->get_map_9();
		int32_t L_3 = V_1;
		int32_t L_4 = V_2;
		MapCell_t3263021842 * L_5 = CMap_getCell_m3922102949(L_2, L_3, L_4, /*hidden argument*/NULL);
		MapCellData_t826838626 * L_6 = L_5->get_data_0();
		V_3 = L_6;
		V_4 = 0;
		goto IL_00b8;
	}

IL_003d:
	{
		MapCellData_t826838626 * L_7 = V_3;
		Int32U5BU5D_t3030399641* L_8 = L_7->get_tiles_3();
		int32_t L_9 = V_4;
		int32_t L_10 = L_9;
		int32_t L_11 = (L_8)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_10));
		if (L_11)
		{
			goto IL_0050;
		}
	}
	{
		goto IL_00b2;
	}

IL_0050:
	{
		MapCellData_t826838626 * L_12 = V_3;
		Int32U5BU5D_t3030399641* L_13 = L_12->get_tiles_3();
		int32_t L_14 = V_4;
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_15));
		int32_t L_17 = EchoTest_getTextureIndex_m624472290(__this, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) >= ((int32_t)0)))
		{
			goto IL_006c;
		}
	}
	{
		goto IL_00b2;
	}

IL_006c:
	{
		List_1_t1612747451 * L_19 = __this->get_t_12();
		int32_t L_20 = V_0;
		Texture_t2243626319 * L_21 = List_1_get_Item_m3764379494(L_19, L_20, /*hidden argument*/List_1_get_Item_m3764379494_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00b2;
		}
	}
	{
		int32_t L_23 = V_1;
		int32_t L_24 = V_2;
		Rect_t3681755626  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Rect__ctor_m1220545469(&L_25, (((float)((float)((int32_t)((int32_t)L_23*(int32_t)((int32_t)64)))))), (((float)((float)((int32_t)((int32_t)L_24*(int32_t)((int32_t)64)))))), (64.0f), (64.0f), /*hidden argument*/NULL);
		List_1_t1612747451 * L_26 = __this->get_t_12();
		int32_t L_27 = V_0;
		Texture_t2243626319 * L_28 = List_1_get_Item_m3764379494(L_26, L_27, /*hidden argument*/List_1_get_Item_m3764379494_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m1191587896(NULL /*static, unused*/, L_25, L_28, /*hidden argument*/NULL);
		goto IL_00b2;
	}

IL_00b2:
	{
		int32_t L_29 = V_4;
		V_4 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00b8:
	{
		int32_t L_30 = V_4;
		if ((((int32_t)L_30) <= ((int32_t)2)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_31 = V_2;
		V_2 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_00c4:
	{
		int32_t L_32 = V_2;
		CMap_t632723465 * L_33 = __this->get_map_9();
		int32_t L_34 = CMap_get_height_m661569650(L_33, /*hidden argument*/NULL);
		if ((((int32_t)L_32) < ((int32_t)L_34)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_35 = V_1;
		V_1 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00d9:
	{
		int32_t L_36 = V_1;
		CMap_t632723465 * L_37 = __this->get_map_9();
		int32_t L_38 = CMap_get_width_m2154012433(L_37, /*hidden argument*/NULL);
		if ((((int32_t)L_36) < ((int32_t)L_38)))
		{
			goto IL_001b;
		}
	}
	{
		return;
	}
}
// System.Int32 EchoTest::getTextureIndex(System.Int32)
extern const MethodInfo* List_1_GetEnumerator_m2527786909_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1062633493_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4282865897_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1274756239_MethodInfo_var;
extern const uint32_t EchoTest_getTextureIndex_m624472290_MetadataUsageId;
extern "C"  int32_t EchoTest_getTextureIndex_m624472290 (EchoTest_t3020015217 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_getTextureIndex_m624472290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Enumerator_t975728254  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		List_1_t1440998580 * L_0 = __this->get_ids_13();
		Enumerator_t975728254  L_1 = List_1_GetEnumerator_m2527786909(L_0, /*hidden argument*/List_1_GetEnumerator_m2527786909_MethodInfo_var);
		V_2 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_0013:
		{
			int32_t L_2 = Enumerator_get_Current_m1062633493((&V_2), /*hidden argument*/Enumerator_get_Current_m1062633493_MethodInfo_var);
			V_1 = L_2;
			int32_t L_3 = V_1;
			int32_t L_4 = ___index0;
			if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
			{
				goto IL_0029;
			}
		}

IL_0022:
		{
			int32_t L_5 = V_0;
			V_3 = L_5;
			IL2CPP_LEAVE(0x4E, FINALLY_003e);
		}

IL_0029:
		{
			int32_t L_6 = V_0;
			V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
		}

IL_002d:
		{
			bool L_7 = Enumerator_MoveNext_m4282865897((&V_2), /*hidden argument*/Enumerator_MoveNext_m4282865897_MethodInfo_var);
			if (L_7)
			{
				goto IL_0013;
			}
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x4C, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1274756239((&V_2), /*hidden argument*/Enumerator_Dispose_m1274756239_MethodInfo_var);
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_JUMP_TBL(0x4C, IL_004c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004c:
	{
		return (-1);
	}

IL_004e:
	{
		int32_t L_8 = V_3;
		return L_8;
	}
}
// System.Int32 EchoTest::findIndex(MapCellData,System.Int32)
extern "C"  int32_t EchoTest_findIndex_m3032432620 (EchoTest_t3020015217 * __this, MapCellData_t826838626 * ___data0, int32_t ___layer1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___layer1;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = ___layer1;
		MapCellData_t826838626 * L_2 = ___data0;
		Int32U5BU5D_t3030399641* L_3 = L_2->get_tiles_3();
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		MapCellData_t826838626 * L_4 = ___data0;
		Int32U5BU5D_t3030399641* L_5 = L_4->get_tiles_3();
		int32_t L_6 = ___layer1;
		int32_t L_7 = L_6;
		int32_t L_8 = (L_5)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7));
		return L_8;
	}

IL_001e:
	{
		return (-1);
	}
}
// System.Void EchoTest::onConnect()
extern Il2CppCodeGenString* _stringLiteral2838059549;
extern const uint32_t EchoTest_onConnect_m417626763_MetadataUsageId;
extern "C"  void EchoTest_onConnect_m417626763 (EchoTest_t3020015217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_onConnect_m417626763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EchoTest_send_m1990524934(__this, _stringLiteral2838059549, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EchoTest::send(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3210189823;
extern Il2CppCodeGenString* _stringLiteral3189639733;
extern const uint32_t EchoTest_send_m1990524934_MetadataUsageId;
extern "C"  void EchoTest_send_m1990524934 (EchoTest_t3020015217 * __this, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_send_m1990524934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_simulateServer_18();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		WebSocket_t1213274227 * L_1 = __this->get_w_17();
		String_t* L_2 = ___s0;
		WebSocket_SendString_m4293857515(L_1, L_2, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		String_t* L_3 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3210189823, L_3, _stringLiteral3189639733, /*hidden argument*/NULL);
		Application_ExternalEval_m2388671608(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EchoTest::onRawMessage(System.Byte[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* EchoTest_t3020015217_il2cpp_TypeInfo_var;
extern Il2CppClass* CMap_t632723465_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m1040667509_MethodInfo_var;
extern const MethodInfo* List_1_Contains_m793041670_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1039294670;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern Il2CppCodeGenString* _stringLiteral1686085084;
extern Il2CppCodeGenString* _stringLiteral372029398;
extern Il2CppCodeGenString* _stringLiteral882449595;
extern Il2CppCodeGenString* _stringLiteral3740682426;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern Il2CppCodeGenString* _stringLiteral1193422053;
extern Il2CppCodeGenString* _stringLiteral3967562426;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern Il2CppCodeGenString* _stringLiteral314625184;
extern Il2CppCodeGenString* _stringLiteral626503817;
extern Il2CppCodeGenString* _stringLiteral2966598941;
extern Il2CppCodeGenString* _stringLiteral793747223;
extern Il2CppCodeGenString* _stringLiteral1097494145;
extern Il2CppCodeGenString* _stringLiteral4162680594;
extern Il2CppCodeGenString* _stringLiteral4190635300;
extern Il2CppCodeGenString* _stringLiteral2307234140;
extern Il2CppCodeGenString* _stringLiteral922837250;
extern const uint32_t EchoTest_onRawMessage_m309866093_MetadataUsageId;
extern "C"  void EchoTest_onRawMessage_m309866093 (EchoTest_t3020015217 * __this, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_onRawMessage_m309866093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ReplyJSONDataArray_t1061980569 * V_3 = NULL;
	StringU5BU5D_t1642385972* V_4 = NULL;
	int32_t V_5 = 0;
	Int32U5BU5D_t3030399641* V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	StringU5BU5D_t1642385972* V_10 = NULL;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	ByteU5BU5D_t3397334013* V_14 = NULL;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	MapCell_t3263021842 * V_21 = NULL;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_1 = Encoding_get_ASCII_m2727409419(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_2 = ___bytes0;
		String_t* L_3 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_1, L_2);
		V_0 = L_3;
		List_1_t1398341365 * L_4 = __this->get_lines_7();
		int32_t L_5 = List_1_get_Count_m780127360(L_4, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(EchoTest_t3020015217_il2cpp_TypeInfo_var);
		int32_t L_6 = ((EchoTest_t3020015217_StaticFields*)EchoTest_t3020015217_il2cpp_TypeInfo_var->static_fields)->get_MAXLINES_8();
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			goto IL_0033;
		}
	}
	{
		List_1_t1398341365 * L_7 = __this->get_lines_7();
		List_1_RemoveAt_m1040667509(L_7, 0, /*hidden argument*/List_1_RemoveAt_m1040667509_MethodInfo_var);
	}

IL_0033:
	{
		String_t* L_8 = V_0;
		bool L_9 = String_StartsWith_m1841920685(L_8, _stringLiteral1039294670, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0069;
		}
	}
	{
		CMap_t632723465 * L_10 = __this->get_map_9();
		int32_t L_11 = CMap_get_width_m2154012433(L_10, /*hidden argument*/NULL);
		CMap_t632723465 * L_12 = __this->get_map_9();
		int32_t L_13 = CMap_get_height_m661569650(L_12, /*hidden argument*/NULL);
		CMap_t632723465 * L_14 = (CMap_t632723465 *)il2cpp_codegen_object_new(CMap_t632723465_il2cpp_TypeInfo_var);
		CMap__ctor_m554309044(L_14, L_11, L_13, /*hidden argument*/NULL);
		__this->set_map_9(L_14);
		goto IL_0635;
	}

IL_0069:
	{
		String_t* L_15 = V_0;
		bool L_16 = String_StartsWith_m1841920685(L_15, _stringLiteral3617362, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00c0;
		}
	}
	{
		CMap_t632723465 * L_17 = __this->get_map_9();
		int32_t L_18 = CMap_get_width_m2154012433(L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		String_t* L_19 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		CMap_t632723465 * L_20 = __this->get_map_9();
		int32_t L_21 = CMap_get_height_m661569650(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		String_t* L_22 = Int32_ToString_m2960866144((&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral1686085084, L_19, _stringLiteral372029398, L_22, /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0635;
	}

IL_00c0:
	{
		String_t* L_24 = V_0;
		bool L_25 = String_StartsWith_m1841920685(L_24, _stringLiteral882449595, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00f6;
		}
	}
	{
		int32_t* L_26 = __this->get_address_of_extiNextId_6();
		String_t* L_27 = Int32_ToString_m2960866144(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral3740682426, L_27, _stringLiteral372029425, /*hidden argument*/NULL);
		V_0 = L_28;
		goto IL_0635;
	}

IL_00f6:
	{
		String_t* L_29 = V_0;
		bool L_30 = String_StartsWith_m1841920685(L_29, _stringLiteral1193422053, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0266;
		}
	}
	{
		String_t* L_31 = V_0;
		bool L_32 = String_Contains_m4017059963(L_31, _stringLiteral3967562426, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_025b;
		}
	}
	{
		String_t* L_33 = V_0;
		String_t* L_34 = String_Substring_m2032624251(L_33, 4, /*hidden argument*/NULL);
		ReplyJSONDataArray_t1061980569 * L_35 = ReplyJSONDataArray_fromDeliantraJSON_m506824200(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		V_3 = L_35;
		ReplyJSONDataArray_t1061980569 * L_36 = V_3;
		StringU5BU5D_t1642385972* L_37 = L_36->get_data_0();
		V_4 = L_37;
		StringU5BU5D_t1642385972* L_38 = V_4;
		int32_t L_39 = 0;
		String_t* L_40 = (L_38)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_39));
		bool L_41 = String_Contains_m4017059963(L_40, _stringLiteral372029313, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0256;
		}
	}
	{
		StringU5BU5D_t1642385972* L_42 = V_4;
		int32_t L_43 = 0;
		String_t* L_44 = (L_42)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_43));
		CharU5BU5D_t1328083999* L_45 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		(L_45)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)45));
		StringU5BU5D_t1642385972* L_46 = String_Split_m3326265864(L_44, L_45, /*hidden argument*/NULL);
		int32_t L_47 = 1;
		String_t* L_48 = (L_46)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_47));
		int32_t L_49 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		V_5 = L_49;
		V_6 = (Int32U5BU5D_t3030399641*)NULL;
		int32_t L_50 = V_5;
		if ((((int32_t)L_50) <= ((int32_t)((int32_t)100))))
		{
			goto IL_019e;
		}
	}
	{
		StringU5BU5D_t1642385972* L_51 = V_4;
		V_6 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_51)->max_length))))-(int32_t)1))));
		V_7 = 1;
		goto IL_0193;
	}

IL_017c:
	{
		Int32U5BU5D_t3030399641* L_52 = V_6;
		int32_t L_53 = V_7;
		StringU5BU5D_t1642385972* L_54 = V_4;
		int32_t L_55 = V_7;
		int32_t L_56 = L_55;
		String_t* L_57 = (L_54)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_56));
		int32_t L_58 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		(L_52)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_53-(int32_t)1))), (int32_t)L_58);
		int32_t L_59 = V_7;
		V_7 = ((int32_t)((int32_t)L_59+(int32_t)1));
	}

IL_0193:
	{
		int32_t L_60 = V_7;
		StringU5BU5D_t1642385972* L_61 = V_4;
		if ((((int32_t)L_60) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_61)->max_length)))))))
		{
			goto IL_017c;
		}
	}

IL_019e:
	{
		int32_t L_62 = V_5;
		if (((int32_t)((int32_t)L_62-(int32_t)((int32_t)100))) == 0)
		{
			goto IL_01b9;
		}
		if (((int32_t)((int32_t)L_62-(int32_t)((int32_t)100))) == 1)
		{
			goto IL_0209;
		}
		if (((int32_t)((int32_t)L_62-(int32_t)((int32_t)100))) == 2)
		{
			goto IL_0240;
		}
	}
	{
		goto IL_024b;
	}

IL_01b9:
	{
		StringU5BU5D_t1642385972* L_63 = V_4;
		int32_t L_64 = 1;
		String_t* L_65 = (L_63)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_64));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_66 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_67 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral314625184, L_65, L_66, /*hidden argument*/NULL);
		__this->set_baseURL_4(L_67);
		int32_t L_68 = __this->get_extiNextId_6();
		int32_t L_69 = ((int32_t)((int32_t)L_68+(int32_t)1));
		V_8 = L_69;
		__this->set_extiNextId_6(L_69);
		int32_t L_70 = V_8;
		V_8 = L_70;
		String_t* L_71 = Int32_ToString_m2960866144((&V_8), /*hidden argument*/NULL);
		String_t* L_72 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral626503817, L_71, _stringLiteral2966598941, /*hidden argument*/NULL);
		V_0 = L_72;
		goto IL_0256;
	}

IL_0209:
	{
		int32_t L_73 = __this->get_extiNextId_6();
		int32_t L_74 = ((int32_t)((int32_t)L_73+(int32_t)1));
		V_9 = L_74;
		__this->set_extiNextId_6(L_74);
		int32_t L_75 = V_9;
		V_9 = L_75;
		String_t* L_76 = Int32_ToString_m2960866144((&V_9), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_77 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral626503817, L_76, _stringLiteral793747223, /*hidden argument*/NULL);
		V_0 = L_77;
		goto IL_0256;
	}

IL_0240:
	{
		V_0 = _stringLiteral1097494145;
		goto IL_0256;
	}

IL_024b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_78 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_78;
		goto IL_0256;
	}

IL_0256:
	{
		goto IL_0261;
	}

IL_025b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_79 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_79;
	}

IL_0261:
	{
		goto IL_0635;
	}

IL_0266:
	{
		String_t* L_80 = V_0;
		bool L_81 = String_StartsWith_m1841920685(L_80, _stringLiteral4162680594, /*hidden argument*/NULL);
		if (!L_81)
		{
			goto IL_0281;
		}
	}
	{
		V_0 = _stringLiteral4190635300;
		goto IL_0635;
	}

IL_0281:
	{
		String_t* L_82 = V_0;
		bool L_83 = String_StartsWith_m1841920685(L_82, _stringLiteral2307234140, /*hidden argument*/NULL);
		if (!L_83)
		{
			goto IL_02dd;
		}
	}
	{
		String_t* L_84 = V_0;
		CharU5BU5D_t1328083999* L_85 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		(L_85)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)32));
		StringU5BU5D_t1642385972* L_86 = String_Split_m3326265864(L_84, L_85, /*hidden argument*/NULL);
		V_10 = L_86;
		StringU5BU5D_t1642385972* L_87 = V_10;
		int32_t L_88 = 1;
		String_t* L_89 = (L_87)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_88));
		int32_t L_90 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		V_11 = L_90;
		StringU5BU5D_t1642385972* L_91 = V_10;
		int32_t L_92 = 2;
		String_t* L_93 = (L_91)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_92));
		int32_t L_94 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		V_12 = L_94;
		int32_t L_95 = __this->get_dx_15();
		int32_t L_96 = V_11;
		__this->set_dx_15(((int32_t)((int32_t)L_95+(int32_t)L_96)));
		int32_t L_97 = __this->get_dy_16();
		int32_t L_98 = V_12;
		__this->set_dy_16(((int32_t)((int32_t)L_97+(int32_t)L_98)));
		goto IL_0635;
	}

IL_02dd:
	{
		String_t* L_99 = V_0;
		bool L_100 = String_StartsWith_m1841920685(L_99, _stringLiteral922837250, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_062f;
		}
	}
	{
		EchoTest_scroll_map_m2763410202(__this, /*hidden argument*/NULL);
		int32_t L_101 = 0;
		V_13 = L_101;
		__this->set_dy_16(L_101);
		int32_t L_102 = V_13;
		__this->set_dx_15(L_102);
		ByteU5BU5D_t3397334013* L_103 = ___bytes0;
		V_14 = L_103;
		V_15 = 6;
		V_16 = 0;
		V_17 = 0;
		goto IL_0617;
	}

IL_0316:
	{
		ByteU5BU5D_t3397334013* L_104 = V_14;
		int32_t L_105 = V_15;
		int32_t L_106 = L_105;
		V_15 = ((int32_t)((int32_t)L_106+(int32_t)1));
		int32_t L_107 = L_106;
		uint8_t L_108 = (L_104)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_107));
		V_18 = ((int32_t)((int32_t)L_108<<(int32_t)8));
		int32_t L_109 = V_18;
		ByteU5BU5D_t3397334013* L_110 = V_14;
		int32_t L_111 = V_15;
		int32_t L_112 = L_111;
		V_15 = ((int32_t)((int32_t)L_112+(int32_t)1));
		int32_t L_113 = L_112;
		uint8_t L_114 = (L_110)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_113));
		V_18 = ((int32_t)((int32_t)L_109+(int32_t)L_114));
		int32_t L_115 = V_18;
		V_19 = ((int32_t)((int32_t)((int32_t)((int32_t)L_115>>(int32_t)((int32_t)10)))&(int32_t)((int32_t)63)));
		int32_t L_116 = V_18;
		V_20 = ((int32_t)((int32_t)((int32_t)((int32_t)L_116>>(int32_t)4))&(int32_t)((int32_t)63)));
		V_16 = 0;
		V_17 = 0;
		CMap_t632723465 * L_117 = __this->get_map_9();
		int32_t L_118 = V_19;
		int32_t L_119 = V_20;
		MapCell_t3263021842 * L_120 = CMap_getCell_m3922102949(L_117, L_118, L_119, /*hidden argument*/NULL);
		V_21 = L_120;
		int32_t L_121 = V_18;
		if (((int32_t)((int32_t)L_121&(int32_t)((int32_t)15))))
		{
			goto IL_0378;
		}
	}
	{
		MapCell_t3263021842 * L_122 = V_21;
		MapCellData_t826838626 * L_123 = L_122->get_data_0();
		MapCellData_clear_m1366406508(L_123, /*hidden argument*/NULL);
		goto IL_0617;
	}

IL_0378:
	{
		MapCell_t3263021842 * L_124 = V_21;
		MapCellData_t826838626 * L_125 = L_124->get_data_0();
		int32_t L_126 = L_125->get_darkness_0();
		if (L_126)
		{
			goto IL_039a;
		}
	}
	{
		MapCell_t3263021842 * L_127 = V_21;
		MapCellData_t826838626 * L_128 = L_127->get_data_0();
		L_128->set_darkness_0(((int32_t)256));
	}

IL_039a:
	{
		int32_t L_129 = V_18;
		if (!((int32_t)((int32_t)L_129&(int32_t)8)))
		{
			goto IL_0589;
		}
	}

IL_03a3:
	{
		ByteU5BU5D_t3397334013* L_130 = V_14;
		int32_t L_131 = V_15;
		int32_t L_132 = L_131;
		V_15 = ((int32_t)((int32_t)L_132+(int32_t)1));
		int32_t L_133 = L_132;
		uint8_t L_134 = (L_130)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_133));
		V_17 = L_134;
		int32_t L_135 = V_17;
		V_16 = ((int32_t)((int32_t)L_135&(int32_t)((int32_t)127)));
		int32_t L_136 = V_16;
		if ((((int32_t)L_136) >= ((int32_t)4)))
		{
			goto IL_03e9;
		}
	}
	{
		MapCell_t3263021842 * L_137 = V_21;
		MapCellData_t826838626 * L_138 = L_137->get_data_0();
		int32_t L_139 = V_17;
		L_138->set_darkness_0(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)255)-(int32_t)((int32_t)((int32_t)L_139*(int32_t)((int32_t)64)))))+(int32_t)1)));
		MapCell_t3263021842 * L_140 = V_21;
		MapCellData_t826838626 * L_141 = L_140->get_data_0();
		L_141->set_isDirty_7((bool)1);
		goto IL_057c;
	}

IL_03e9:
	{
		int32_t L_142 = V_16;
		if ((!(((uint32_t)L_142) == ((uint32_t)5))))
		{
			goto IL_0426;
		}
	}
	{
		MapCell_t3263021842 * L_143 = V_21;
		MapCellData_t826838626 * L_144 = L_143->get_data_0();
		L_144->set_stat_width_5(1);
		MapCell_t3263021842 * L_145 = V_21;
		MapCellData_t826838626 * L_146 = L_145->get_data_0();
		ByteU5BU5D_t3397334013* L_147 = V_14;
		int32_t L_148 = V_15;
		int32_t L_149 = L_148;
		V_15 = ((int32_t)((int32_t)L_149+(int32_t)1));
		int32_t L_150 = L_149;
		uint8_t L_151 = (L_147)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_150));
		L_146->set_stat_hp_6(L_151);
		MapCell_t3263021842 * L_152 = V_21;
		MapCellData_t826838626 * L_153 = L_152->get_data_0();
		L_153->set_isDirty_7((bool)1);
		goto IL_057c;
	}

IL_0426:
	{
		int32_t L_154 = V_16;
		if ((!(((uint32_t)L_154) == ((uint32_t)6))))
		{
			goto IL_0458;
		}
	}
	{
		MapCell_t3263021842 * L_155 = V_21;
		MapCellData_t826838626 * L_156 = L_155->get_data_0();
		ByteU5BU5D_t3397334013* L_157 = V_14;
		int32_t L_158 = V_15;
		int32_t L_159 = L_158;
		V_15 = ((int32_t)((int32_t)L_159+(int32_t)1));
		int32_t L_160 = L_159;
		uint8_t L_161 = (L_157)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_160));
		L_156->set_stat_width_5(((int32_t)((int32_t)L_161+(int32_t)1)));
		MapCell_t3263021842 * L_162 = V_21;
		MapCellData_t826838626 * L_163 = L_162->get_data_0();
		L_163->set_isDirty_7((bool)1);
		goto IL_057c;
	}

IL_0458:
	{
		int32_t L_164 = V_16;
		if ((!(((uint32_t)L_164) == ((uint32_t)((int32_t)71)))))
		{
			goto IL_0538;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_165 = V_14;
		int32_t L_166 = V_15;
		int32_t L_167 = L_166;
		uint8_t L_168 = (L_165)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_167));
		if ((!(((uint32_t)L_168) == ((uint32_t)1))))
		{
			goto IL_0484;
		}
	}
	{
		MapCell_t3263021842 * L_169 = V_21;
		MapCellData_t826838626 * L_170 = L_169->get_data_0();
		ByteU5BU5D_t3397334013* L_171 = V_14;
		int32_t L_172 = V_15;
		int32_t L_173 = ((int32_t)((int32_t)L_172+(int32_t)1));
		uint8_t L_174 = (L_171)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_173));
		L_170->set_player_2(L_174);
		goto IL_0527;
	}

IL_0484:
	{
		ByteU5BU5D_t3397334013* L_175 = V_14;
		int32_t L_176 = V_15;
		int32_t L_177 = L_176;
		uint8_t L_178 = (L_175)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_177));
		if ((!(((uint32_t)L_178) == ((uint32_t)2))))
		{
			goto IL_04b1;
		}
	}
	{
		MapCell_t3263021842 * L_179 = V_21;
		MapCellData_t826838626 * L_180 = L_179->get_data_0();
		ByteU5BU5D_t3397334013* L_181 = V_14;
		int32_t L_182 = V_15;
		int32_t L_183 = ((int32_t)((int32_t)L_182+(int32_t)2));
		uint8_t L_184 = (L_181)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_183));
		ByteU5BU5D_t3397334013* L_185 = V_14;
		int32_t L_186 = V_15;
		int32_t L_187 = ((int32_t)((int32_t)L_186+(int32_t)1));
		uint8_t L_188 = (L_185)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_187));
		L_180->set_player_2(((int32_t)((int32_t)L_184+(int32_t)((int32_t)((int32_t)L_188<<(int32_t)8)))));
		goto IL_0527;
	}

IL_04b1:
	{
		ByteU5BU5D_t3397334013* L_189 = V_14;
		int32_t L_190 = V_15;
		int32_t L_191 = L_190;
		uint8_t L_192 = (L_189)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_191));
		if ((!(((uint32_t)L_192) == ((uint32_t)3))))
		{
			goto IL_04e9;
		}
	}
	{
		MapCell_t3263021842 * L_193 = V_21;
		MapCellData_t826838626 * L_194 = L_193->get_data_0();
		ByteU5BU5D_t3397334013* L_195 = V_14;
		int32_t L_196 = V_15;
		int32_t L_197 = ((int32_t)((int32_t)L_196+(int32_t)3));
		uint8_t L_198 = (L_195)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_197));
		ByteU5BU5D_t3397334013* L_199 = V_14;
		int32_t L_200 = V_15;
		int32_t L_201 = ((int32_t)((int32_t)L_200+(int32_t)2));
		uint8_t L_202 = (L_199)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_201));
		ByteU5BU5D_t3397334013* L_203 = V_14;
		int32_t L_204 = V_15;
		int32_t L_205 = ((int32_t)((int32_t)L_204+(int32_t)1));
		uint8_t L_206 = (L_203)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_205));
		L_194->set_player_2(((int32_t)((int32_t)((int32_t)((int32_t)L_198+(int32_t)((int32_t)((int32_t)L_202<<(int32_t)8))))+(int32_t)((int32_t)((int32_t)L_206<<(int32_t)((int32_t)16))))));
		goto IL_0527;
	}

IL_04e9:
	{
		ByteU5BU5D_t3397334013* L_207 = V_14;
		int32_t L_208 = V_15;
		int32_t L_209 = L_208;
		uint8_t L_210 = (L_207)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_209));
		if ((!(((uint32_t)L_210) == ((uint32_t)4))))
		{
			goto IL_0527;
		}
	}
	{
		MapCell_t3263021842 * L_211 = V_21;
		MapCellData_t826838626 * L_212 = L_211->get_data_0();
		ByteU5BU5D_t3397334013* L_213 = V_14;
		int32_t L_214 = V_15;
		int32_t L_215 = ((int32_t)((int32_t)L_214+(int32_t)4));
		uint8_t L_216 = (L_213)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_215));
		ByteU5BU5D_t3397334013* L_217 = V_14;
		int32_t L_218 = V_15;
		int32_t L_219 = ((int32_t)((int32_t)L_218+(int32_t)3));
		uint8_t L_220 = (L_217)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_219));
		ByteU5BU5D_t3397334013* L_221 = V_14;
		int32_t L_222 = V_15;
		int32_t L_223 = ((int32_t)((int32_t)L_222+(int32_t)2));
		uint8_t L_224 = (L_221)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_223));
		ByteU5BU5D_t3397334013* L_225 = V_14;
		int32_t L_226 = V_15;
		int32_t L_227 = ((int32_t)((int32_t)L_226+(int32_t)1));
		uint8_t L_228 = (L_225)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_227));
		L_212->set_player_2(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_216+(int32_t)((int32_t)((int32_t)L_220<<(int32_t)8))))+(int32_t)((int32_t)((int32_t)L_224<<(int32_t)((int32_t)16)))))+(int32_t)((int32_t)((int32_t)L_228<<(int32_t)((int32_t)24))))));
	}

IL_0527:
	{
		int32_t L_229 = V_15;
		ByteU5BU5D_t3397334013* L_230 = V_14;
		int32_t L_231 = V_15;
		int32_t L_232 = L_231;
		uint8_t L_233 = (L_230)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_232));
		V_15 = ((int32_t)((int32_t)L_229+(int32_t)((int32_t)((int32_t)L_233+(int32_t)1))));
		goto IL_057c;
	}

IL_0538:
	{
		int32_t L_234 = V_16;
		if ((!(((uint32_t)L_234) == ((uint32_t)8))))
		{
			goto IL_055b;
		}
	}
	{
		MapCell_t3263021842 * L_235 = V_21;
		MapCellData_t826838626 * L_236 = L_235->get_data_0();
		ByteU5BU5D_t3397334013* L_237 = V_14;
		int32_t L_238 = V_15;
		int32_t L_239 = L_238;
		V_15 = ((int32_t)((int32_t)L_239+(int32_t)1));
		int32_t L_240 = L_239;
		uint8_t L_241 = (L_237)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_240));
		L_236->set_flags_1(L_241);
		goto IL_057c;
	}

IL_055b:
	{
		int32_t L_242 = V_17;
		if (!((int32_t)((int32_t)L_242&(int32_t)((int32_t)64))))
		{
			goto IL_0576;
		}
	}
	{
		int32_t L_243 = V_15;
		ByteU5BU5D_t3397334013* L_244 = V_14;
		int32_t L_245 = V_15;
		int32_t L_246 = L_245;
		uint8_t L_247 = (L_244)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_246));
		V_15 = ((int32_t)((int32_t)L_243+(int32_t)((int32_t)((int32_t)L_247+(int32_t)1))));
		goto IL_057c;
	}

IL_0576:
	{
		int32_t L_248 = V_15;
		V_15 = ((int32_t)((int32_t)L_248+(int32_t)1));
	}

IL_057c:
	{
		int32_t L_249 = V_17;
		if (((int32_t)((int32_t)L_249&(int32_t)((int32_t)128))))
		{
			goto IL_03a3;
		}
	}

IL_0589:
	{
		MapCell_t3263021842 * L_250 = V_21;
		MapCellData_t826838626 * L_251 = L_250->get_data_0();
		L_251->set_tilesCount_4(0);
		V_22 = 0;
		goto IL_060f;
	}

IL_059e:
	{
		int32_t L_252 = V_18;
		int32_t L_253 = V_22;
		if (!((int32_t)((int32_t)L_252&(int32_t)((int32_t)((int32_t)4>>(int32_t)((int32_t)((int32_t)L_253&(int32_t)((int32_t)31))))))))
		{
			goto IL_0609;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_254 = V_14;
		int32_t L_255 = V_15;
		int32_t L_256 = L_255;
		V_15 = ((int32_t)((int32_t)L_256+(int32_t)1));
		int32_t L_257 = L_256;
		uint8_t L_258 = (L_254)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_257));
		V_23 = ((int32_t)((int32_t)L_258<<(int32_t)8));
		int32_t L_259 = V_23;
		ByteU5BU5D_t3397334013* L_260 = V_14;
		int32_t L_261 = V_15;
		int32_t L_262 = L_261;
		V_15 = ((int32_t)((int32_t)L_262+(int32_t)1));
		int32_t L_263 = L_262;
		uint8_t L_264 = (L_260)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_263));
		V_23 = ((int32_t)((int32_t)L_259+(int32_t)L_264));
		int32_t L_265 = V_23;
		if (!L_265)
		{
			goto IL_05eb;
		}
	}
	{
		List_1_t1440998580 * L_266 = __this->get_ids_13();
		int32_t L_267 = V_23;
		bool L_268 = List_1_Contains_m793041670(L_266, L_267, /*hidden argument*/List_1_Contains_m793041670_MethodInfo_var);
		if (L_268)
		{
			goto IL_05eb;
		}
	}
	{
		int32_t L_269 = V_23;
		EchoTest_need_facenum_m3893050015(__this, L_269, /*hidden argument*/NULL);
	}

IL_05eb:
	{
		MapCell_t3263021842 * L_270 = V_21;
		MapCellData_t826838626 * L_271 = L_270->get_data_0();
		Int32U5BU5D_t3030399641* L_272 = L_271->get_tiles_3();
		int32_t L_273 = V_22;
		int32_t L_274 = V_23;
		(L_272)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_273), (int32_t)L_274);
		MapCell_t3263021842 * L_275 = V_21;
		MapCellData_t826838626 * L_276 = L_275->get_data_0();
		L_276->set_isDirty_7((bool)1);
	}

IL_0609:
	{
		int32_t L_277 = V_22;
		V_22 = ((int32_t)((int32_t)L_277+(int32_t)1));
	}

IL_060f:
	{
		int32_t L_278 = V_22;
		if ((((int32_t)L_278) <= ((int32_t)2)))
		{
			goto IL_059e;
		}
	}

IL_0617:
	{
		int32_t L_279 = V_15;
		ByteU5BU5D_t3397334013* L_280 = V_14;
		if ((((int32_t)L_279) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_280)->max_length))))-(int32_t)2)))))
		{
			goto IL_0316;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_281 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_281;
		goto IL_0635;
	}

IL_062f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_282 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_282;
	}

IL_0635:
	{
		String_t* L_283 = V_0;
		int32_t L_284 = String_get_Length_m1606060069(L_283, /*hidden argument*/NULL);
		if ((((int32_t)L_284) <= ((int32_t)0)))
		{
			goto IL_0648;
		}
	}
	{
		String_t* L_285 = V_0;
		EchoTest_send_m1990524934(__this, L_285, /*hidden argument*/NULL);
	}

IL_0648:
	{
		return;
	}
}
// System.Void EchoTest::onMessage(System.String)
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t EchoTest_onMessage_m3738535102_MetadataUsageId;
extern "C"  void EchoTest_onMessage_m3738535102 (EchoTest_t3020015217 * __this, String_t* ___encodedMessage0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_onMessage_m3738535102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		String_t* L_0 = ___encodedMessage0;
		CharU5BU5D_t1328083999* L_1 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		StringU5BU5D_t1642385972* L_2 = String_Split_m3326265864(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		StringU5BU5D_t1642385972* L_3 = V_0;
		V_1 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))));
		V_2 = 0;
		goto IL_0034;
	}

IL_0022:
	{
		StringU5BU5D_t1642385972* L_4 = V_0;
		int32_t L_5 = V_2;
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		int32_t L_8 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		ByteU5BU5D_t3397334013* L_9 = V_1;
		int32_t L_10 = V_2;
		int32_t L_11 = V_3;
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_10), (uint8_t)(((int32_t)((uint8_t)L_11))));
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_13 = V_2;
		ByteU5BU5D_t3397334013* L_14 = V_1;
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0022;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_15 = V_1;
		EchoTest_onRawMessage_m309866093(__this, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EchoTest::scroll_map()
extern Il2CppClass* MapRow_t137873350_il2cpp_TypeInfo_var;
extern Il2CppClass* MapCell_t3263021842_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_RemoveAt_m1195871569_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1116316207_MethodInfo_var;
extern const MethodInfo* List_1_Insert_m4116967226_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3386062531_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3655811018_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m489059213_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2789292135_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m4257738323_MethodInfo_var;
extern const MethodInfo* List_1_Insert_m3266941434_MethodInfo_var;
extern const uint32_t EchoTest_scroll_map_m2763410202_MetadataUsageId;
extern "C"  void EchoTest_scroll_map_m2763410202 (EchoTest_t3020015217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_scroll_map_m2763410202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		int32_t L_0 = __this->get_dx_15();
		V_0 = L_0;
		int32_t L_1 = __this->get_dy_16();
		V_1 = L_1;
		V_2 = 0;
		CMap_t632723465 * L_2 = __this->get_map_9();
		int32_t L_3 = CMap_get_width_m2154012433(L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		CMap_t632723465 * L_4 = __this->get_map_9();
		int32_t L_5 = CMap_get_height_m661569650(L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		goto IL_0059;
	}

IL_002e:
	{
		CMap_t632723465 * L_6 = __this->get_map_9();
		List_1_t3801961778 * L_7 = L_6->get_rows_0();
		List_1_RemoveAt_m1195871569(L_7, 0, /*hidden argument*/List_1_RemoveAt_m1195871569_MethodInfo_var);
		CMap_t632723465 * L_8 = __this->get_map_9();
		List_1_t3801961778 * L_9 = L_8->get_rows_0();
		int32_t L_10 = V_3;
		MapRow_t137873350 * L_11 = (MapRow_t137873350 *)il2cpp_codegen_object_new(MapRow_t137873350_il2cpp_TypeInfo_var);
		MapRow__ctor_m3625560204(L_11, L_10, /*hidden argument*/NULL);
		List_1_Add_m1116316207(L_9, L_11, /*hidden argument*/List_1_Add_m1116316207_MethodInfo_var);
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0059:
	{
		int32_t L_13 = V_2;
		int32_t L_14 = V_1;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_002e;
		}
	}
	{
		V_2 = 0;
		goto IL_00a4;
	}

IL_0067:
	{
		CMap_t632723465 * L_15 = __this->get_map_9();
		List_1_t3801961778 * L_16 = L_15->get_rows_0();
		int32_t L_17 = V_3;
		MapRow_t137873350 * L_18 = (MapRow_t137873350 *)il2cpp_codegen_object_new(MapRow_t137873350_il2cpp_TypeInfo_var);
		MapRow__ctor_m3625560204(L_18, L_17, /*hidden argument*/NULL);
		List_1_Insert_m4116967226(L_16, 0, L_18, /*hidden argument*/List_1_Insert_m4116967226_MethodInfo_var);
		CMap_t632723465 * L_19 = __this->get_map_9();
		List_1_t3801961778 * L_20 = L_19->get_rows_0();
		CMap_t632723465 * L_21 = __this->get_map_9();
		List_1_t3801961778 * L_22 = L_21->get_rows_0();
		int32_t L_23 = List_1_get_Count_m3386062531(L_22, /*hidden argument*/List_1_get_Count_m3386062531_MethodInfo_var);
		List_1_RemoveAt_m1195871569(L_20, ((int32_t)((int32_t)L_23-(int32_t)1)), /*hidden argument*/List_1_RemoveAt_m1195871569_MethodInfo_var);
		int32_t L_24 = V_2;
		V_2 = ((int32_t)((int32_t)L_24-(int32_t)1));
	}

IL_00a4:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = V_2;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0067;
		}
	}
	{
		V_2 = 0;
		goto IL_010b;
	}

IL_00b2:
	{
		V_5 = 0;
		goto IL_00fe;
	}

IL_00ba:
	{
		CMap_t632723465 * L_27 = __this->get_map_9();
		List_1_t3801961778 * L_28 = L_27->get_rows_0();
		int32_t L_29 = V_5;
		MapRow_t137873350 * L_30 = List_1_get_Item_m3655811018(L_28, L_29, /*hidden argument*/List_1_get_Item_m3655811018_MethodInfo_var);
		List_1_t2632142974 * L_31 = L_30->get_cells_0();
		List_1_RemoveAt_m489059213(L_31, 0, /*hidden argument*/List_1_RemoveAt_m489059213_MethodInfo_var);
		CMap_t632723465 * L_32 = __this->get_map_9();
		List_1_t3801961778 * L_33 = L_32->get_rows_0();
		int32_t L_34 = V_5;
		MapRow_t137873350 * L_35 = List_1_get_Item_m3655811018(L_33, L_34, /*hidden argument*/List_1_get_Item_m3655811018_MethodInfo_var);
		List_1_t2632142974 * L_36 = L_35->get_cells_0();
		MapCell_t3263021842 * L_37 = (MapCell_t3263021842 *)il2cpp_codegen_object_new(MapCell_t3263021842_il2cpp_TypeInfo_var);
		MapCell__ctor_m1307241529(L_37, /*hidden argument*/NULL);
		List_1_Add_m2789292135(L_36, L_37, /*hidden argument*/List_1_Add_m2789292135_MethodInfo_var);
		int32_t L_38 = V_5;
		V_5 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00fe:
	{
		int32_t L_39 = V_5;
		int32_t L_40 = V_4;
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_00ba;
		}
	}
	{
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_010b:
	{
		int32_t L_42 = V_2;
		int32_t L_43 = V_0;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_00b2;
		}
	}
	{
		V_2 = 0;
		goto IL_0190;
	}

IL_0119:
	{
		V_6 = 0;
		goto IL_0183;
	}

IL_0121:
	{
		CMap_t632723465 * L_44 = __this->get_map_9();
		List_1_t3801961778 * L_45 = L_44->get_rows_0();
		int32_t L_46 = V_6;
		MapRow_t137873350 * L_47 = List_1_get_Item_m3655811018(L_45, L_46, /*hidden argument*/List_1_get_Item_m3655811018_MethodInfo_var);
		List_1_t2632142974 * L_48 = L_47->get_cells_0();
		CMap_t632723465 * L_49 = __this->get_map_9();
		List_1_t3801961778 * L_50 = L_49->get_rows_0();
		int32_t L_51 = V_6;
		MapRow_t137873350 * L_52 = List_1_get_Item_m3655811018(L_50, L_51, /*hidden argument*/List_1_get_Item_m3655811018_MethodInfo_var);
		List_1_t2632142974 * L_53 = L_52->get_cells_0();
		int32_t L_54 = List_1_get_Count_m4257738323(L_53, /*hidden argument*/List_1_get_Count_m4257738323_MethodInfo_var);
		List_1_RemoveAt_m489059213(L_48, ((int32_t)((int32_t)L_54-(int32_t)1)), /*hidden argument*/List_1_RemoveAt_m489059213_MethodInfo_var);
		CMap_t632723465 * L_55 = __this->get_map_9();
		List_1_t3801961778 * L_56 = L_55->get_rows_0();
		int32_t L_57 = V_6;
		MapRow_t137873350 * L_58 = List_1_get_Item_m3655811018(L_56, L_57, /*hidden argument*/List_1_get_Item_m3655811018_MethodInfo_var);
		List_1_t2632142974 * L_59 = L_58->get_cells_0();
		MapCell_t3263021842 * L_60 = (MapCell_t3263021842 *)il2cpp_codegen_object_new(MapCell_t3263021842_il2cpp_TypeInfo_var);
		MapCell__ctor_m1307241529(L_60, /*hidden argument*/NULL);
		List_1_Insert_m3266941434(L_59, 0, L_60, /*hidden argument*/List_1_Insert_m3266941434_MethodInfo_var);
		int32_t L_61 = V_6;
		V_6 = ((int32_t)((int32_t)L_61+(int32_t)1));
	}

IL_0183:
	{
		int32_t L_62 = V_6;
		int32_t L_63 = V_4;
		if ((((int32_t)L_62) < ((int32_t)L_63)))
		{
			goto IL_0121;
		}
	}
	{
		int32_t L_64 = V_2;
		V_2 = ((int32_t)((int32_t)L_64-(int32_t)1));
	}

IL_0190:
	{
		int32_t L_65 = V_0;
		int32_t L_66 = V_2;
		if ((((int32_t)L_65) < ((int32_t)L_66)))
		{
			goto IL_0119;
		}
	}
	{
		return;
	}
}
// System.Void EchoTest::need_facenum(System.Int32)
extern const MethodInfo* List_1_Add_m2828939739_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3972184193_MethodInfo_var;
extern const uint32_t EchoTest_need_facenum_m3893050015_MetadataUsageId;
extern "C"  void EchoTest_need_facenum_m3893050015 (EchoTest_t3020015217 * __this, int32_t ___face0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_need_facenum_m3893050015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1440998580 * L_0 = __this->get_ids_13();
		int32_t L_1 = ___face0;
		List_1_Add_m2828939739(L_0, L_1, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		List_1_t1612747451 * L_2 = __this->get_t_12();
		List_1_Add_m3972184193(L_2, (Texture_t2243626319 *)NULL, /*hidden argument*/List_1_Add_m3972184193_MethodInfo_var);
		return;
	}
}
// System.Void EchoTest::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m852068579_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1635882288;
extern Il2CppCodeGenString* _stringLiteral4246352212;
extern Il2CppCodeGenString* _stringLiteral4246380314;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral103249698;
extern Il2CppCodeGenString* _stringLiteral3477336324;
extern Il2CppCodeGenString* _stringLiteral496108495;
extern Il2CppCodeGenString* _stringLiteral3923005145;
extern Il2CppCodeGenString* _stringLiteral3862581407;
extern const uint32_t EchoTest_Update_m3540047033_MetadataUsageId;
extern "C"  void EchoTest_Update_m3540047033 (EchoTest_t3020015217 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest_Update_m3540047033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	{
		WebSocket_t1213274227 * L_0 = __this->get_w_17();
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		bool L_1 = __this->get_simulateServer_18();
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		bool L_2 = __this->get_bWSConnected_22();
		if (!L_2)
		{
			goto IL_0043;
		}
	}
	{
		WebSocket_t1213274227 * L_3 = __this->get_w_17();
		ByteU5BU5D_t3397334013* L_4 = WebSocket_Recv_m3676309770(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t3397334013* L_5 = V_0;
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_6 = V_0;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0043;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_7 = V_0;
		EchoTest_onRawMessage_m309866093(__this, L_7, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_8 = __this->get_ticks_20();
		__this->set_ticks_20(((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = __this->get_ticks_20();
		if ((((int32_t)L_9) <= ((int32_t)((int32_t)18))))
		{
			goto IL_014b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_10 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		if ((!(((float)L_10) > ((float)(0.0f)))))
		{
			goto IL_0089;
		}
	}
	{
		EchoTest_send_m1990524934(__this, _stringLiteral4246352212, /*hidden argument*/NULL);
		__this->set_ticks_20(0);
		goto IL_014b;
	}

IL_0089:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_11 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		if ((!(((float)L_11) < ((float)(0.0f)))))
		{
			goto IL_00b4;
		}
	}
	{
		EchoTest_send_m1990524934(__this, _stringLiteral4246380314, /*hidden argument*/NULL);
		__this->set_ticks_20(0);
		goto IL_014b;
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_12 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		if ((!(((float)L_12) < ((float)(0.0f)))))
		{
			goto IL_00df;
		}
	}
	{
		EchoTest_send_m1990524934(__this, _stringLiteral103249698, /*hidden argument*/NULL);
		__this->set_ticks_20(0);
		goto IL_014b;
	}

IL_00df:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_13 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		if ((!(((float)L_13) > ((float)(0.0f)))))
		{
			goto IL_010a;
		}
	}
	{
		EchoTest_send_m1990524934(__this, _stringLiteral3477336324, /*hidden argument*/NULL);
		__this->set_ticks_20(0);
		goto IL_014b;
	}

IL_010a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_14 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_012d;
		}
	}
	{
		EchoTest_send_m1990524934(__this, _stringLiteral496108495, /*hidden argument*/NULL);
		__this->set_ticks_20(0);
		goto IL_014b;
	}

IL_012d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_15 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)44), /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_014b;
		}
	}
	{
		EchoTest_send_m1990524934(__this, _stringLiteral3923005145, /*hidden argument*/NULL);
		__this->set_ticks_20(0);
	}

IL_014b:
	{
		int32_t L_16 = __this->get_numthreads_19();
		if ((((int32_t)L_16) <= ((int32_t)((int32_t)20))))
		{
			goto IL_0159;
		}
	}
	{
		return;
	}

IL_0159:
	{
		int32_t L_17 = __this->get_currentFaceNum_21();
		List_1_t1440998580 * L_18 = __this->get_ids_13();
		int32_t L_19 = List_1_get_Count_m852068579(L_18, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0170;
		}
	}
	{
		return;
	}

IL_0170:
	{
		List_1_t1440998580 * L_20 = __this->get_ids_13();
		int32_t L_21 = __this->get_currentFaceNum_21();
		int32_t L_22 = List_1_get_Item_m1921196075(L_20, L_21, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		if ((((int32_t)L_22) < ((int32_t)0)))
		{
			goto IL_01aa;
		}
	}
	{
		List_1_t1440998580 * L_23 = __this->get_ids_13();
		int32_t L_24 = __this->get_currentFaceNum_21();
		int32_t L_25 = List_1_get_Item_m1921196075(L_23, L_24, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		FacesJSONObject_t1129027889 * L_26 = __this->get_facesJSON_10();
		FacesJSONEntryObjectU5BU5D_t2881799652* L_27 = L_26->get_data_0();
		if ((((int32_t)L_25) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_27)->max_length)))))))
		{
			goto IL_01ab;
		}
	}

IL_01aa:
	{
		return;
	}

IL_01ab:
	{
		List_1_t1440998580 * L_28 = __this->get_ids_13();
		int32_t L_29 = __this->get_currentFaceNum_21();
		int32_t L_30 = List_1_get_Item_m1921196075(L_28, L_29, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_1 = L_30;
		int32_t L_31 = V_1;
		String_t* L_32 = EchoTest_findCsum_m2560875761(__this, L_31, /*hidden argument*/NULL);
		V_2 = L_32;
		String_t* L_33 = V_2;
		if (L_33)
		{
			goto IL_01cc;
		}
	}
	{
		return;
	}

IL_01cc:
	{
		String_t* L_34 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3862581407, L_34, /*hidden argument*/NULL);
		int32_t L_36 = __this->get_currentFaceNum_21();
		Il2CppObject * L_37 = EchoTest_FetchImage_m1536496846(__this, L_35, L_36, /*hidden argument*/NULL);
		V_3 = L_37;
		Il2CppObject * L_38 = V_3;
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_38, /*hidden argument*/NULL);
		int32_t L_39 = __this->get_currentFaceNum_21();
		__this->set_currentFaceNum_21(((int32_t)((int32_t)L_39+(int32_t)1)));
		return;
	}
}
// System.String EchoTest::findCsum(System.Int32)
extern "C"  String_t* EchoTest_findCsum_m2560875761 (EchoTest_t3020015217 * __this, int32_t ___id0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0036;
	}

IL_0007:
	{
		FacesJSONObject_t1129027889 * L_0 = __this->get_facesJSON_10();
		FacesJSONEntryObjectU5BU5D_t2881799652* L_1 = L_0->get_data_0();
		int32_t L_2 = V_0;
		int32_t L_3 = L_2;
		FacesJSONEntryObject_t727229337 * L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		int32_t L_5 = L_4->get_id_0();
		int32_t L_6 = ___id0;
		if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
		{
			goto IL_0032;
		}
	}
	{
		FacesJSONObject_t1129027889 * L_7 = __this->get_facesJSON_10();
		FacesJSONEntryObjectU5BU5D_t2881799652* L_8 = L_7->get_data_0();
		int32_t L_9 = V_0;
		int32_t L_10 = L_9;
		FacesJSONEntryObject_t727229337 * L_11 = (L_8)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_10));
		String_t* L_12 = L_11->get_csum_1();
		return L_12;
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_14 = V_0;
		FacesJSONObject_t1129027889 * L_15 = __this->get_facesJSON_10();
		FacesJSONEntryObjectU5BU5D_t2881799652* L_16 = L_15->get_data_0();
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return (String_t*)NULL;
	}
}
// System.Void EchoTest::.cctor()
extern Il2CppClass* EchoTest_t3020015217_il2cpp_TypeInfo_var;
extern const uint32_t EchoTest__cctor_m2199241283_MetadataUsageId;
extern "C"  void EchoTest__cctor_m2199241283 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EchoTest__cctor_m2199241283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((EchoTest_t3020015217_StaticFields*)EchoTest_t3020015217_il2cpp_TypeInfo_var->static_fields)->set_MAXLINES_8(((int32_t)14));
		return;
	}
}
// System.Void EchoTest/<FetchFacesJson>c__Iterator1::.ctor()
extern "C"  void U3CFetchFacesJsonU3Ec__Iterator1__ctor_m3122957467 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean EchoTest/<FetchFacesJson>c__Iterator1::MoveNext()
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchFacesJsonU3Ec__Iterator1_MoveNext_m3697603421_MetadataUsageId;
extern "C"  bool U3CFetchFacesJsonU3Ec__Iterator1_MoveNext_m3697603421 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchFacesJsonU3Ec__Iterator1_MoveNext_m3697603421_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0052;
		}
	}
	{
		goto IL_006f;
	}

IL_0021:
	{
		String_t* L_2 = __this->get_url_0();
		WWW_t2919945039 * L_3 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_1(L_3);
		WWW_t2919945039 * L_4 = __this->get_U3CwwwU3E__0_1();
		__this->set_U24current_3(L_4);
		bool L_5 = __this->get_U24disposing_4();
		if (L_5)
		{
			goto IL_004d;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_004d:
	{
		goto IL_0071;
	}

IL_0052:
	{
		EchoTest_t3020015217 * L_6 = __this->get_U24this_2();
		WWW_t2919945039 * L_7 = __this->get_U3CwwwU3E__0_1();
		String_t* L_8 = WWW_get_text_m1558985139(L_7, /*hidden argument*/NULL);
		EchoTest_onFacesJSONLoaded_m1919704972(L_6, L_8, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_006f:
	{
		return (bool)0;
	}

IL_0071:
	{
		return (bool)1;
	}
}
// System.Object EchoTest/<FetchFacesJson>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchFacesJsonU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2500433069 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object EchoTest/<FetchFacesJson>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchFacesJsonU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3506911445 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void EchoTest/<FetchFacesJson>c__Iterator1::Dispose()
extern "C"  void U3CFetchFacesJsonU3Ec__Iterator1_Dispose_m2411797382 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void EchoTest/<FetchFacesJson>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchFacesJsonU3Ec__Iterator1_Reset_m240333128_MetadataUsageId;
extern "C"  void U3CFetchFacesJsonU3Ec__Iterator1_Reset_m240333128 (U3CFetchFacesJsonU3Ec__Iterator1_t4119425628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchFacesJsonU3Ec__Iterator1_Reset_m240333128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EchoTest/<FetchImage>c__Iterator3::.ctor()
extern "C"  void U3CFetchImageU3Ec__Iterator3__ctor_m3868575392 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean EchoTest/<FetchImage>c__Iterator3::MoveNext()
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_set_Item_m1688056201_MethodInfo_var;
extern const uint32_t U3CFetchImageU3Ec__Iterator3_MoveNext_m3570818484_MetadataUsageId;
extern "C"  bool U3CFetchImageU3Ec__Iterator3_MoveNext_m3570818484 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchImageU3Ec__Iterator3_MoveNext_m3570818484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_00a0;
	}

IL_0021:
	{
		EchoTest_t3020015217 * L_2 = __this->get_U24this_3();
		EchoTest_t3020015217 * L_3 = L_2;
		int32_t L_4 = L_3->get_numthreads_19();
		L_3->set_numthreads_19(((int32_t)((int32_t)L_4+(int32_t)1)));
		String_t* L_5 = __this->get_url_0();
		WWW_t2919945039 * L_6 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_1(L_6);
		WWW_t2919945039 * L_7 = __this->get_U3CwwwU3E__0_1();
		__this->set_U24current_4(L_7);
		bool L_8 = __this->get_U24disposing_5();
		if (L_8)
		{
			goto IL_0060;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0060:
	{
		goto IL_00a2;
	}

IL_0065:
	{
		EchoTest_t3020015217 * L_9 = __this->get_U24this_3();
		List_1_t1612747451 * L_10 = L_9->get_t_12();
		int32_t L_11 = __this->get_index_2();
		WWW_t2919945039 * L_12 = __this->get_U3CwwwU3E__0_1();
		Texture2D_t3542995729 * L_13 = WWW_get_texture_m1121178301(L_12, /*hidden argument*/NULL);
		List_1_set_Item_m1688056201(L_10, L_11, L_13, /*hidden argument*/List_1_set_Item_m1688056201_MethodInfo_var);
		EchoTest_t3020015217 * L_14 = __this->get_U24this_3();
		EchoTest_t3020015217 * L_15 = L_14;
		int32_t L_16 = L_15->get_numthreads_19();
		L_15->set_numthreads_19(((int32_t)((int32_t)L_16-(int32_t)1)));
		__this->set_U24PC_6((-1));
	}

IL_00a0:
	{
		return (bool)0;
	}

IL_00a2:
	{
		return (bool)1;
	}
}
// System.Object EchoTest/<FetchImage>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchImageU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1489611034 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object EchoTest/<FetchImage>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchImageU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m478534082 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void EchoTest/<FetchImage>c__Iterator3::Dispose()
extern "C"  void U3CFetchImageU3Ec__Iterator3_Dispose_m1978909059 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void EchoTest/<FetchImage>c__Iterator3::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchImageU3Ec__Iterator3_Reset_m1839717077_MetadataUsageId;
extern "C"  void U3CFetchImageU3Ec__Iterator3_Reset_m1839717077 (U3CFetchImageU3Ec__Iterator3_t3816779753 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchImageU3Ec__Iterator3_Reset_m1839717077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EchoTest/<FetchPacketsJson>c__Iterator2::.ctor()
extern "C"  void U3CFetchPacketsJsonU3Ec__Iterator2__ctor_m181738545 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean EchoTest/<FetchPacketsJson>c__Iterator2::MoveNext()
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchPacketsJsonU3Ec__Iterator2_MoveNext_m1219194327_MetadataUsageId;
extern "C"  bool U3CFetchPacketsJsonU3Ec__Iterator2_MoveNext_m1219194327 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchPacketsJsonU3Ec__Iterator2_MoveNext_m1219194327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0052;
		}
	}
	{
		goto IL_006f;
	}

IL_0021:
	{
		String_t* L_2 = __this->get_url_0();
		WWW_t2919945039 * L_3 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_1(L_3);
		WWW_t2919945039 * L_4 = __this->get_U3CwwwU3E__0_1();
		__this->set_U24current_3(L_4);
		bool L_5 = __this->get_U24disposing_4();
		if (L_5)
		{
			goto IL_004d;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_004d:
	{
		goto IL_0071;
	}

IL_0052:
	{
		EchoTest_t3020015217 * L_6 = __this->get_U24this_2();
		WWW_t2919945039 * L_7 = __this->get_U3CwwwU3E__0_1();
		String_t* L_8 = WWW_get_text_m1558985139(L_7, /*hidden argument*/NULL);
		EchoTest_onPacketsJSONLoaded_m216094539(L_6, L_8, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_006f:
	{
		return (bool)0;
	}

IL_0071:
	{
		return (bool)1;
	}
}
// System.Object EchoTest/<FetchPacketsJson>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFetchPacketsJsonU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3990172979 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object EchoTest/<FetchPacketsJson>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFetchPacketsJsonU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m700220187 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void EchoTest/<FetchPacketsJson>c__Iterator2::Dispose()
extern "C"  void U3CFetchPacketsJsonU3Ec__Iterator2_Dispose_m1200506034 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void EchoTest/<FetchPacketsJson>c__Iterator2::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CFetchPacketsJsonU3Ec__Iterator2_Reset_m3434169572_MetadataUsageId;
extern "C"  void U3CFetchPacketsJsonU3Ec__Iterator2_Reset_m3434169572 (U3CFetchPacketsJsonU3Ec__Iterator2_t3529262072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFetchPacketsJsonU3Ec__Iterator2_Reset_m3434169572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EchoTest/<onWSConnected>c__Iterator0::.ctor()
extern "C"  void U3ConWSConnectedU3Ec__Iterator0__ctor_m1156059884 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean EchoTest/<onWSConnected>c__Iterator0::MoveNext()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3455429553;
extern Il2CppCodeGenString* _stringLiteral244025437;
extern const uint32_t U3ConWSConnectedU3Ec__Iterator0_MoveNext_m1025350420_MetadataUsageId;
extern "C"  bool U3ConWSConnectedU3Ec__Iterator0_MoveNext_m1025350420 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3ConWSConnectedU3Ec__Iterator0_MoveNext_m1025350420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0060;
		}
	}
	{
		goto IL_009d;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3455429553, /*hidden argument*/NULL);
		EchoTest_t3020015217 * L_2 = __this->get_U24this_0();
		EchoTest_t3020015217 * L_3 = __this->get_U24this_0();
		WebSocket_t1213274227 * L_4 = L_3->get_w_17();
		Il2CppObject * L_5 = WebSocket_Connect_m2620683194(L_4, /*hidden argument*/NULL);
		Coroutine_t2299508840 * L_6 = MonoBehaviour_StartCoroutine_m2470621050(L_2, L_5, /*hidden argument*/NULL);
		__this->set_U24current_1(L_6);
		bool L_7 = __this->get_U24disposing_2();
		if (L_7)
		{
			goto IL_005b;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_005b:
	{
		goto IL_009f;
	}

IL_0060:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral244025437, /*hidden argument*/NULL);
		EchoTest_t3020015217 * L_8 = __this->get_U24this_0();
		WebSocket_t1213274227 * L_9 = L_8->get_w_17();
		String_t* L_10 = WebSocket_get_Error_m2795227660(L_9, /*hidden argument*/NULL);
		Debug_Log_m920475918(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		EchoTest_t3020015217 * L_11 = __this->get_U24this_0();
		L_11->set_bWSConnected_22((bool)1);
		EchoTest_t3020015217 * L_12 = __this->get_U24this_0();
		EchoTest_onConnect_m417626763(L_12, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_009d:
	{
		return (bool)0;
	}

IL_009f:
	{
		return (bool)1;
	}
}
// System.Object EchoTest/<onWSConnected>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3ConWSConnectedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m402253988 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object EchoTest/<onWSConnected>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3ConWSConnectedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m657078236 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void EchoTest/<onWSConnected>c__Iterator0::Dispose()
extern "C"  void U3ConWSConnectedU3Ec__Iterator0_Dispose_m3882673845 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void EchoTest/<onWSConnected>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3ConWSConnectedU3Ec__Iterator0_Reset_m78835803_MetadataUsageId;
extern "C"  void U3ConWSConnectedU3Ec__Iterator0_Reset_m78835803 (U3ConWSConnectedU3Ec__Iterator0_t570499943 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3ConWSConnectedU3Ec__Iterator0_Reset_m78835803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void FacesJSONEntryObject::.ctor()
extern "C"  void FacesJSONEntryObject__ctor_m2385661870 (FacesJSONEntryObject_t727229337 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FacesJSONObject::.ctor()
extern "C"  void FacesJSONObject__ctor_m2604168514 (FacesJSONObject_t1129027889 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// FacesJSONObject FacesJSONObject::fromJSON(System.String)
extern const MethodInfo* JsonUtility_FromJson_TisFacesJSONObject_t1129027889_m706920862_MethodInfo_var;
extern const uint32_t FacesJSONObject_fromJSON_m3341462460_MetadataUsageId;
extern "C"  FacesJSONObject_t1129027889 * FacesJSONObject_fromJSON_m3341462460 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacesJSONObject_fromJSON_m3341462460_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___json0;
		FacesJSONObject_t1129027889 * L_1 = JsonUtility_FromJson_TisFacesJSONObject_t1129027889_m706920862(NULL /*static, unused*/, L_0, /*hidden argument*/JsonUtility_FromJson_TisFacesJSONObject_t1129027889_m706920862_MethodInfo_var);
		return L_1;
	}
}
// System.Void MapCell::.ctor()
extern Il2CppClass* MapCellData_t826838626_il2cpp_TypeInfo_var;
extern const uint32_t MapCell__ctor_m1307241529_MetadataUsageId;
extern "C"  void MapCell__ctor_m1307241529 (MapCell_t3263021842 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MapCell__ctor_m1307241529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MapCellData_t826838626 * L_0 = (MapCellData_t826838626 *)il2cpp_codegen_object_new(MapCellData_t826838626_il2cpp_TypeInfo_var);
		MapCellData__ctor_m2952753737(L_0, /*hidden argument*/NULL);
		__this->set_data_0(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MapCellData::.ctor()
extern "C"  void MapCellData__ctor_m2952753737 (MapCellData_t826838626 * __this, const MethodInfo* method)
{
	{
		__this->set_isDirty_7((bool)1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		MapCellData_clear_m1366406508(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MapCellData::clear()
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const uint32_t MapCellData_clear_m1366406508_MetadataUsageId;
extern "C"  void MapCellData_clear_m1366406508 (MapCellData_t826838626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MapCellData_clear_m1366406508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_darkness_0(0);
		__this->set_flags_1(0);
		__this->set_player_2(0);
		__this->set_tiles_3(((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_tilesCount_4(0);
		__this->set_stat_width_5(0);
		__this->set_stat_hp_6(0);
		return;
	}
}
// System.Void MapRow::.ctor(System.Int32)
extern Il2CppClass* List_1_t2632142974_il2cpp_TypeInfo_var;
extern Il2CppClass* MapCell_t3263021842_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3083038955_MethodInfo_var;
extern const uint32_t MapRow__ctor_m3625560204_MetadataUsageId;
extern "C"  void MapRow__ctor_m3625560204 (MapRow_t137873350 * __this, int32_t ___destSize0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MapRow__ctor_m3625560204_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2632142974 * L_0 = (List_1_t2632142974 *)il2cpp_codegen_object_new(List_1_t2632142974_il2cpp_TypeInfo_var);
		List_1__ctor_m3083038955(L_0, /*hidden argument*/List_1__ctor_m3083038955_MethodInfo_var);
		__this->set_cells_0(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		goto IL_0021;
	}

IL_0016:
	{
		MapCell_t3263021842 * L_1 = (MapCell_t3263021842 *)il2cpp_codegen_object_new(MapCell_t3263021842_il2cpp_TypeInfo_var);
		MapCell__ctor_m1307241529(L_1, /*hidden argument*/NULL);
		MapRow_addCell_m354249166(__this, L_1, /*hidden argument*/NULL);
	}

IL_0021:
	{
		int32_t L_2 = MapRow_get_length_m3503286366(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___destSize0;
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_0016;
		}
	}
	{
		return;
	}
}
// System.Int32 MapRow::get_length()
extern const MethodInfo* List_1_get_Count_m4257738323_MethodInfo_var;
extern const uint32_t MapRow_get_length_m3503286366_MetadataUsageId;
extern "C"  int32_t MapRow_get_length_m3503286366 (MapRow_t137873350 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MapRow_get_length_m3503286366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2632142974 * L_0 = __this->get_cells_0();
		int32_t L_1 = List_1_get_Count_m4257738323(L_0, /*hidden argument*/List_1_get_Count_m4257738323_MethodInfo_var);
		return L_1;
	}
}
// System.Void MapRow::addCell(MapCell)
extern const MethodInfo* List_1_Add_m2789292135_MethodInfo_var;
extern const uint32_t MapRow_addCell_m354249166_MetadataUsageId;
extern "C"  void MapRow_addCell_m354249166 (MapRow_t137873350 * __this, MapCell_t3263021842 * ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MapRow_addCell_m354249166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MapCell_t3263021842 * V_0 = NULL;
	{
		int32_t L_0 = MapRow_get_length_m3503286366(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002e;
		}
	}
	{
		MapCell_t3263021842 * L_1 = ___c0;
		MapCell_t3263021842 * L_2 = L_1;
		V_0 = L_2;
		__this->set_lastCell_2(L_2);
		MapCell_t3263021842 * L_3 = V_0;
		__this->set_firstCell_1(L_3);
		MapCell_t3263021842 * L_4 = ___c0;
		MapCell_t3263021842 * L_5 = ___c0;
		L_4->set_previous_1(L_5);
		MapCell_t3263021842 * L_6 = ___c0;
		MapCell_t3263021842 * L_7 = ___c0;
		L_6->set_next_2(L_7);
		goto IL_0059;
	}

IL_002e:
	{
		MapCell_t3263021842 * L_8 = ___c0;
		MapCell_t3263021842 * L_9 = __this->get_lastCell_2();
		L_8->set_previous_1(L_9);
		MapCell_t3263021842 * L_10 = ___c0;
		MapCell_t3263021842 * L_11 = __this->get_firstCell_1();
		L_10->set_next_2(L_11);
		MapCell_t3263021842 * L_12 = __this->get_lastCell_2();
		MapCell_t3263021842 * L_13 = ___c0;
		L_12->set_next_2(L_13);
		MapCell_t3263021842 * L_14 = ___c0;
		__this->set_lastCell_2(L_14);
	}

IL_0059:
	{
		List_1_t2632142974 * L_15 = __this->get_cells_0();
		MapCell_t3263021842 * L_16 = ___c0;
		List_1_Add_m2789292135(L_15, L_16, /*hidden argument*/List_1_Add_m2789292135_MethodInfo_var);
		return;
	}
}
// System.Void ReplyJSONDataArray::.ctor()
extern "C"  void ReplyJSONDataArray__ctor_m3167406150 (ReplyJSONDataArray_t1061980569 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// ReplyJSONDataArray ReplyJSONDataArray::fromDeliantraJSON(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisReplyJSONDataArray_t1061980569_m2871469018_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2734328829;
extern Il2CppCodeGenString* _stringLiteral1947253693;
extern const uint32_t ReplyJSONDataArray_fromDeliantraJSON_m506824200_MetadataUsageId;
extern "C"  ReplyJSONDataArray_t1061980569 * ReplyJSONDataArray_fromDeliantraJSON_m506824200 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReplyJSONDataArray_fromDeliantraJSON_m506824200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2734328829, L_0, _stringLiteral1947253693, /*hidden argument*/NULL);
		___json0 = L_1;
		String_t* L_2 = ___json0;
		ReplyJSONDataArray_t1061980569 * L_3 = JsonUtility_FromJson_TisReplyJSONDataArray_t1061980569_m2871469018(NULL /*static, unused*/, L_2, /*hidden argument*/JsonUtility_FromJson_TisReplyJSONDataArray_t1061980569_m2871469018_MethodInfo_var);
		return L_3;
	}
}
// ReplyJSONDataArray ReplyJSONDataArray::fromJSON(System.String)
extern const MethodInfo* JsonUtility_FromJson_TisReplyJSONDataArray_t1061980569_m2871469018_MethodInfo_var;
extern const uint32_t ReplyJSONDataArray_fromJSON_m1557679406_MetadataUsageId;
extern "C"  ReplyJSONDataArray_t1061980569 * ReplyJSONDataArray_fromJSON_m1557679406 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReplyJSONDataArray_fromJSON_m1557679406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___json0;
		ReplyJSONDataArray_t1061980569 * L_1 = JsonUtility_FromJson_TisReplyJSONDataArray_t1061980569_m2871469018(NULL /*static, unused*/, L_0, /*hidden argument*/JsonUtility_FromJson_TisReplyJSONDataArray_t1061980569_m2871469018_MethodInfo_var);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
