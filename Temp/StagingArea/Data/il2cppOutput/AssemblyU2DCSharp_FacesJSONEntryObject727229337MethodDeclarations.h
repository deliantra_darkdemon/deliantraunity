﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FacesJSONEntryObject
struct FacesJSONEntryObject_t727229337;

#include "codegen/il2cpp-codegen.h"

// System.Void FacesJSONEntryObject::.ctor()
extern "C"  void FacesJSONEntryObject__ctor_m2385661870 (FacesJSONEntryObject_t727229337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
