﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// WebSocket
struct WebSocket_t1213274227;
// System.Uri
struct Uri_t19570940;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// WebSocket/<Connect>c__Iterator0
struct U3CConnectU3Ec__Iterator0_t738167129;
// System.Object
struct Il2CppObject;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_WebSocket1213274227.h"
#include "AssemblyU2DCSharpU2Dfirstpass_WebSocket1213274227MethodDeclarations.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "System_System_Uri19570940MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Runtime_InteropServices_Marshal785896760MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_WebSocket_U3CConnectU738167129MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_WebSocket_U3CConnectU738167129.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebSocket::.ctor(System.Uri)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3110053184;
extern Il2CppCodeGenString* _stringLiteral3115737039;
extern Il2CppCodeGenString* _stringLiteral2470049881;
extern const uint32_t WebSocket__ctor_m2447929677_MetadataUsageId;
extern "C"  void WebSocket__ctor_m2447929677 (WebSocket_t1213274227 * __this, Uri_t19570940 * ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket__ctor_m2447929677_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Uri_t19570940 * L_0 = ___url0;
		__this->set_mUrl_0(L_0);
		Uri_t19570940 * L_1 = __this->get_mUrl_0();
		String_t* L_2 = Uri_get_Scheme_m55908894(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		bool L_4 = String_Equals_m2633592423(L_3, _stringLiteral3110053184, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_004a;
		}
	}
	{
		String_t* L_5 = V_0;
		bool L_6 = String_Equals_m2633592423(L_5, _stringLiteral3115737039, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004a;
		}
	}
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2470049881, L_7, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_9 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_004a:
	{
		return;
	}
}
// System.Void WebSocket::SendString(System.String)
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_SendString_m4293857515_MetadataUsageId;
extern "C"  void WebSocket_SendString_m4293857515 (WebSocket_t1213274227 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_SendString_m4293857515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___str0;
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		WebSocket_Send_m2905861795(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.String WebSocket::RecvString()
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_RecvString_m716708100_MetadataUsageId;
extern "C"  String_t* WebSocket_RecvString_m716708100 (WebSocket_t1213274227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_RecvString_m716708100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = WebSocket_Recv_m3676309770(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		ByteU5BU5D_t3397334013* L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_2 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_3 = V_0;
		String_t* L_4 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_2, L_3);
		return L_4;
	}
}
extern "C" int32_t DEFAULT_CALL SocketCreate(intptr_t);
// System.Int32 WebSocket::SocketCreate(System.IntPtr)
extern "C"  int32_t WebSocket_SocketCreate_m984752765 (Il2CppObject * __this /* static, unused */, IntPtr_t ___url0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(SocketCreate)(reinterpret_cast<intptr_t>((___url0).get_m_value_0()));

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL SocketState(int32_t);
// System.Int32 WebSocket::SocketState(System.Int32)
extern "C"  int32_t WebSocket_SocketState_m2287817149 (Il2CppObject * __this /* static, unused */, int32_t ___socketInstance0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(SocketState)(___socketInstance0);

	return returnValue;
}
extern "C" void DEFAULT_CALL SocketSend(int32_t, intptr_t, int32_t);
// System.Void WebSocket::SocketSend(System.Int32,System.IntPtr,System.Int32)
extern "C"  void WebSocket_SocketSend_m3450815743 (Il2CppObject * __this /* static, unused */, int32_t ___socketInstance0, IntPtr_t ___ptr1, int32_t ___length2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SocketSend)(___socketInstance0, reinterpret_cast<intptr_t>((___ptr1).get_m_value_0()), ___length2);

}
extern "C" void DEFAULT_CALL SocketRecv(int32_t, intptr_t, int32_t);
// System.Void WebSocket::SocketRecv(System.Int32,System.IntPtr,System.Int32)
extern "C"  void WebSocket_SocketRecv_m2093096381 (Il2CppObject * __this /* static, unused */, int32_t ___socketInstance0, IntPtr_t ___ptr1, int32_t ___length2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SocketRecv)(___socketInstance0, reinterpret_cast<intptr_t>((___ptr1).get_m_value_0()), ___length2);

}
extern "C" int32_t DEFAULT_CALL SocketRecvLength(int32_t);
// System.Int32 WebSocket::SocketRecvLength(System.Int32)
extern "C"  int32_t WebSocket_SocketRecvLength_m3766772832 (Il2CppObject * __this /* static, unused */, int32_t ___socketInstance0, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(SocketRecvLength)(___socketInstance0);

	return returnValue;
}
extern "C" void DEFAULT_CALL SocketClose(int32_t);
// System.Void WebSocket::SocketClose(System.Int32)
extern "C"  void WebSocket_SocketClose_m4206234618 (Il2CppObject * __this /* static, unused */, int32_t ___socketInstance0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SocketClose)(___socketInstance0);

}
extern "C" int32_t DEFAULT_CALL SocketError(int32_t, intptr_t, int32_t);
// System.Int32 WebSocket::SocketError(System.Int32,System.IntPtr,System.Int32)
extern "C"  int32_t WebSocket_SocketError_m4025441441 (Il2CppObject * __this /* static, unused */, int32_t ___socketInstance0, IntPtr_t ___ptr1, int32_t ___length2, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(SocketError)(___socketInstance0, reinterpret_cast<intptr_t>((___ptr1).get_m_value_0()), ___length2);

	return returnValue;
}
// System.Void WebSocket::Send(System.Byte[])
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_Send_m2905861795_MetadataUsageId;
extern "C"  void WebSocket_Send_m2905861795 (WebSocket_t1213274227 * __this, ByteU5BU5D_t3397334013* ___buffer0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Send_m2905861795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ByteU5BU5D_t3397334013* L_0 = ___buffer0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Marshal_AllocHGlobal_m4258042074(NULL /*static, unused*/, (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_1;
		ByteU5BU5D_t3397334013* L_2 = ___buffer0;
		IntPtr_t L_3 = V_0;
		ByteU5BU5D_t3397334013* L_4 = ___buffer0;
		Marshal_Copy_m3575923452(NULL /*static, unused*/, L_2, 0, L_3, (((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))), /*hidden argument*/NULL);
		int32_t L_5 = __this->get_m_NativeRef_1();
		IntPtr_t L_6 = V_0;
		ByteU5BU5D_t3397334013* L_7 = ___buffer0;
		WebSocket_SocketSend_m3450815743(NULL /*static, unused*/, L_5, L_6, (((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))), /*hidden argument*/NULL);
		IntPtr_t L_8 = V_0;
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] WebSocket::Recv()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_Recv_m3676309770_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* WebSocket_Recv_m3676309770 (WebSocket_t1213274227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Recv_m3676309770_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_m_NativeRef_1();
		int32_t L_1 = WebSocket_SocketRecvLength_m3766772832(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_0014;
		}
	}
	{
		return (ByteU5BU5D_t3397334013*)NULL;
	}

IL_0014:
	{
		int32_t L_3 = V_0;
		V_1 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_3));
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		IntPtr_t L_5 = Marshal_AllocHGlobal_m4258042074(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = __this->get_m_NativeRef_1();
		IntPtr_t L_7 = V_2;
		int32_t L_8 = V_0;
		WebSocket_SocketRecv_m2093096381(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		IntPtr_t L_9 = V_2;
		ByteU5BU5D_t3397334013* L_10 = V_1;
		int32_t L_11 = V_0;
		Marshal_Copy_m1683535972(NULL /*static, unused*/, L_9, L_10, 0, L_11, /*hidden argument*/NULL);
		IntPtr_t L_12 = V_2;
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_13 = V_1;
		return L_13;
	}
}
// System.Collections.IEnumerator WebSocket::Connect()
extern Il2CppClass* U3CConnectU3Ec__Iterator0_t738167129_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_Connect_m2620683194_MetadataUsageId;
extern "C"  Il2CppObject * WebSocket_Connect_m2620683194 (WebSocket_t1213274227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_Connect_m2620683194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CConnectU3Ec__Iterator0_t738167129 * V_0 = NULL;
	{
		U3CConnectU3Ec__Iterator0_t738167129 * L_0 = (U3CConnectU3Ec__Iterator0_t738167129 *)il2cpp_codegen_object_new(U3CConnectU3Ec__Iterator0_t738167129_il2cpp_TypeInfo_var);
		U3CConnectU3Ec__Iterator0__ctor_m280435780(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CConnectU3Ec__Iterator0_t738167129 * L_1 = V_0;
		L_1->set_U24this_1(__this);
		U3CConnectU3Ec__Iterator0_t738167129 * L_2 = V_0;
		return L_2;
	}
}
// System.Void WebSocket::Close()
extern "C"  void WebSocket_Close_m13439692 (WebSocket_t1213274227 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_NativeRef_1();
		WebSocket_SocketClose_m4206234618(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String WebSocket::get_Error()
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const uint32_t WebSocket_get_Error_m2795227660_MetadataUsageId;
extern "C"  String_t* WebSocket_get_Error_m2795227660 (WebSocket_t1213274227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebSocket_get_Error_m2795227660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		IntPtr_t L_0 = Marshal_AllocHGlobal_m4258042074(NULL /*static, unused*/, ((int32_t)1024), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = __this->get_m_NativeRef_1();
		IntPtr_t L_2 = V_0;
		int32_t L_3 = WebSocket_SocketError_m4025441441(NULL /*static, unused*/, L_1, L_2, ((int32_t)1024), /*hidden argument*/NULL);
		V_1 = L_3;
		V_2 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)1024)));
		IntPtr_t L_4 = V_0;
		ByteU5BU5D_t3397334013* L_5 = V_2;
		Marshal_Copy_m1683535972(NULL /*static, unused*/, L_4, L_5, 0, ((int32_t)1024), /*hidden argument*/NULL);
		IntPtr_t L_6 = V_0;
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		if (L_7)
		{
			goto IL_0043;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_8 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_9 = V_2;
		String_t* L_10 = VirtFuncInvoker1< String_t*, ByteU5BU5D_t3397334013* >::Invoke(22 /* System.String System.Text.Encoding::GetString(System.Byte[]) */, L_8, L_9);
		return L_10;
	}
}
// System.Void WebSocket/<Connect>c__Iterator0::.ctor()
extern "C"  void U3CConnectU3Ec__Iterator0__ctor_m280435780 (U3CConnectU3Ec__Iterator0_t738167129 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WebSocket/<Connect>c__Iterator0::MoveNext()
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t U3CConnectU3Ec__Iterator0_MoveNext_m3108191136_MetadataUsageId;
extern "C"  bool U3CConnectU3Ec__Iterator0_MoveNext_m3108191136 (U3CConnectU3Ec__Iterator0_t738167129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CConnectU3Ec__Iterator0_MoveNext_m3108191136_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0082;
		}
	}
	{
		goto IL_009e;
	}

IL_0021:
	{
		WebSocket_t1213274227 * L_2 = __this->get_U24this_1();
		Uri_t19570940 * L_3 = L_2->get_mUrl_0();
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		IntPtr_t L_5 = Marshal_StringToHGlobalAnsi_m2621479009(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_U3CstringPtrU3E__0_0(L_5);
		WebSocket_t1213274227 * L_6 = __this->get_U24this_1();
		IntPtr_t L_7 = __this->get_U3CstringPtrU3E__0_0();
		int32_t L_8 = WebSocket_SocketCreate_m984752765(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		L_6->set_m_NativeRef_1(L_8);
		IntPtr_t L_9 = __this->get_U3CstringPtrU3E__0_0();
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_0082;
	}

IL_0062:
	{
		int32_t L_10 = 0;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		__this->set_U24current_2(L_11);
		bool L_12 = __this->get_U24disposing_3();
		if (L_12)
		{
			goto IL_007d;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_007d:
	{
		goto IL_00a0;
	}

IL_0082:
	{
		WebSocket_t1213274227 * L_13 = __this->get_U24this_1();
		int32_t L_14 = L_13->get_m_NativeRef_1();
		int32_t L_15 = WebSocket_SocketState_m2287817149(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0062;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		return (bool)1;
	}
}
// System.Object WebSocket/<Connect>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CConnectU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1854700170 (U3CConnectU3Ec__Iterator0_t738167129 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object WebSocket/<Connect>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CConnectU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m381433762 (U3CConnectU3Ec__Iterator0_t738167129 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void WebSocket/<Connect>c__Iterator0::Dispose()
extern "C"  void U3CConnectU3Ec__Iterator0_Dispose_m368446881 (U3CConnectU3Ec__Iterator0_t738167129 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void WebSocket/<Connect>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CConnectU3Ec__Iterator0_Reset_m2887649615_MetadataUsageId;
extern "C"  void U3CConnectU3Ec__Iterator0_Reset_m2887649615 (U3CConnectU3Ec__Iterator0_t738167129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CConnectU3Ec__Iterator0_Reset_m2887649615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
