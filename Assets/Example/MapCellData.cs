﻿/*
 * This file is part of Deliantra Unity Client.
 *
 * Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra Unity CLient is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */


public class MapCellData
{



    public int darkness;
    public int flags;
    public int player;
    public int[] tiles;
    public int tilesCount;
    public int stat_width;
    public int stat_hp;

    public bool isDirty = true;

    public MapCellData()
    {
        clear();
    }

    public void clear()
    {
        darkness = 0;
        flags = 0;
        player = 0;
        tiles = new int[4];
        tilesCount = 0;
        stat_width = 0;
        stat_hp = 0;
    }
}