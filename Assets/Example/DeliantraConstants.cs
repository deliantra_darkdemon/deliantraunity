﻿/*
 * This file is part of Deliantra Unity Client.
 *
 * Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra Unity CLient is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class DeliantraConstants
{
    public static readonly int CS_QUERY_YESNO = 0x1;
    public static readonly int CS_QUERY_SINGLECHAR = 0x2;
    public static readonly int CS_QUERY_HIDEINPUT = 0x4;
    public static readonly int CS_SAY_NORMAL = 0x1;
    public static readonly int CS_SAY_SHOUT = 0x2;
    public static readonly int CS_SAY_GSAY = 0x4;
    public static readonly float FLOAT_MULTI = 100000;
    public static readonly float FLOAT_MULTF = 100000.0f;
    public static readonly int CS_STAT_HP = 1;
    public static readonly int CS_STAT_MAXHP = 2;
    public static readonly int CS_STAT_SP = 3;
    public static readonly int CS_STAT_MAXSP = 4;
    public static readonly int CS_STAT_STR = 5;
    public static readonly int CS_STAT_INT = 6;
    public static readonly int CS_STAT_WIS = 7;
    public static readonly int CS_STAT_DEX = 8;
    public static readonly int CS_STAT_CON = 9;
    public static readonly int CS_STAT_CHA = 10;
    public static readonly int CS_STAT_EXP = 11;
    public static readonly int CS_STAT_LEVEL = 12;
    public static readonly int CS_STAT_WC = 13;
    public static readonly int CS_STAT_AC = 14;
    public static readonly int CS_STAT_DAM = 15;
    public static readonly int CS_STAT_ARMOUR = 16;
    public static readonly int CS_STAT_SPEED = 17;
    public static readonly int CS_STAT_FOOD = 18;
    public static readonly int CS_STAT_WEAP_SP = 19;
    public static readonly int CS_STAT_RANGE = 20;
    public static readonly int CS_STAT_TITLE = 21;
    public static readonly int CS_STAT_POW = 22;
    public static readonly int CS_STAT_GRACE = 23;
    public static readonly int CS_STAT_MAXGRACE = 24;
    public static readonly int CS_STAT_FLAGS = 25;
    public static readonly int CS_STAT_WEIGHT_LIM = 26;
    public static readonly int CS_STAT_EXP64 = 28;
    public static readonly int CS_STAT_SPELL_ATTUNE = 29;
    public static readonly int CS_STAT_SPELL_REPEL = 30;
    public static readonly int CS_STAT_SPELL_DENY = 31;
    public static readonly int CS_STAT_RESIST_START = 100;
    public static readonly int CS_STAT_RESIST_END = 117;
    public static readonly int CS_STAT_RES_PHYS = 100;
    public static readonly int CS_STAT_RES_MAG = 101;
    public static readonly int CS_STAT_RES_FIRE = 102;
    public static readonly int CS_STAT_RES_ELEC = 103;
    public static readonly int CS_STAT_RES_COLD = 104;
    public static readonly int CS_STAT_RES_CONF = 105;
    public static readonly int CS_STAT_RES_ACID = 106;
    public static readonly int CS_STAT_RES_DRAIN = 107;
    public static readonly int CS_STAT_RES_GHOSTHIT = 108;
    public static readonly int CS_STAT_RES_POISON = 109;
    public static readonly int CS_STAT_RES_SLOW = 110;
    public static readonly int CS_STAT_RES_PARA = 111;
    public static readonly int CS_STAT_TURN_UNDEAD = 112;
    public static readonly int CS_STAT_RES_FEAR = 113;
    public static readonly int CS_STAT_RES_DEPLETE = 114;
    public static readonly int CS_STAT_RES_DEATH = 115;
    public static readonly int CS_STAT_RES_HOLYWORD = 116;
    public static readonly int CS_STAT_RES_BLIND = 117;
    public static readonly int CS_STAT_SKILLINFO = 140;
    public static readonly int CS_NUM_SKILLS = 50;
    public static readonly int SF_FIREON = 0x01;
    public static readonly int SF_RUNON = 0x02;
    public static readonly int NDI_BLACK = 0;
    public static readonly int NDI_WHITE = 1;
    public static readonly int NDI_NAVY = 2;
    public static readonly int NDI_RED = 3;
    public static readonly int NDI_ORANGE = 4;
    public static readonly int NDI_BLUE = 5;
    public static readonly int NDI_DK_ORANGE = 6;
    public static readonly int NDI_GREEN = 7;
    public static readonly int NDI_LT_GREEN = 8;
    public static readonly int NDI_GREY = 9;
    public static readonly int NDI_BROWN = 10;
    public static readonly int NDI_GOLD = 11;
    public static readonly int NDI_TAN = 12;
    public static readonly int NDI_MAX_COLOR = 12;
    public static readonly int NDI_COLOR_MASK = 0x1f;
    public static readonly int NDI_REPLY = 0x20;
    public static readonly int NDI_NOCRATE = 0x40;
    public static readonly int NDI_CLEAR = 0x80;
    public static readonly int a_none = 0;
    public static readonly int a_readied = 1;
    public static readonly int a_wielded = 2;
    public static readonly int a_worn = 3;
    public static readonly int a_active = 4;
    public static readonly int a_applied = 5;
    public static readonly int F_APPLIED = 0x000F;
    public static readonly int F_LOCATION = 0x00F0;
    public static readonly int F_UNPAID = 0x0200;
    public static readonly int F_MAGIC = 0x0400;
    public static readonly int F_CURSED = 0x0800;
    public static readonly int F_DAMNED = 0x1000;
    public static readonly int F_OPEN = 0x2000;
    public static readonly int F_NOPICK = 0x4000;
    public static readonly int F_LOCKED = 0x8000;
    public static readonly int CF_FACE_NONE = 0;
    public static readonly int CF_FACE_BITMAP = 1;
    public static readonly int CF_FACE_XPM = 2;
    public static readonly int CF_FACE_PNG = 3;
    public static readonly int CF_FACE_CACHE = 0x10;
    public static readonly int FACE_FLOOR = 0x80;
    public static readonly int FACE_COLOR_MASK = 0xf;
    public static readonly int UPD_LOCATION = 0x01;
    public static readonly int UPD_FLAGS = 0x02;
    public static readonly int UPD_WEIGHT = 0x04;
    public static readonly int UPD_FACE = 0x08;
    public static readonly int UPD_NAME = 0x10;
    public static readonly int UPD_ANIM = 0x20;
    public static readonly int UPD_ANIMSPEED = 0x40;
    public static readonly int UPD_NROF = 0x80;
    public static readonly int UPD_SP_MANA = 0x01;
    public static readonly int UPD_SP_GRACE = 0x02;
    public static readonly int UPD_SP_LEVEL = 0x04;
    public static readonly int SOUND_NORMAL = 0;
    public static readonly int SOUND_SPELL = 1;

    public static readonly int PICKUP_NOTHING = 0x00000000;

    public static readonly int PICKUP_DEBUG = 0x10000000;
    public static readonly int PICKUP_INHIBIT = 0x20000000;
    public static readonly int PICKUP_STOP = 0x40000000;
    public static readonly uint PICKUP_NEWMODE = 0x80000000;

    public static readonly int PICKUP_RATIO = 0x0000000F;

    public static readonly int PICKUP_FOOD = 0x00000010;
    public static readonly int PICKUP_DRINK = 0x00000020;
    public static readonly int PICKUP_VALUABLES = 0x00000040;
    public static readonly int PICKUP_BOW = 0x00000080;

    public static readonly int PICKUP_ARROW = 0x00000100;
    public static readonly int PICKUP_HELMET = 0x00000200;
    public static readonly int PICKUP_SHIELD = 0x00000400;
    public static readonly int PICKUP_ARMOUR = 0x00000800;

    public static readonly int PICKUP_BOOTS = 0x00001000;
    public static readonly int PICKUP_GLOVES = 0x00002000;
    public static readonly int PICKUP_CLOAK = 0x00004000;
    public static readonly int PICKUP_KEY = 0x00008000;

    public static readonly int PICKUP_MISSILEWEAPON = 0x00010000;
    public static readonly int PICKUP_ALLWEAPON = 0x00020000;
    public static readonly int PICKUP_MAGICAL = 0x00040000;
    public static readonly int PICKUP_POTION = 0x00080000;

    public static readonly int PICKUP_SPELLBOOK = 0x00100000;
    public static readonly int PICKUP_SKILLSCROLL = 0x00200000;
    public static readonly int PICKUP_READABLES = 0x00400000;
    public static readonly int PICKUP_MAGIC_DEVICE = 0x00800000;

    public static readonly int PICKUP_NOT_CURSED = 0x01000000;

    public static readonly int PICKUP_JEWELS = 0x02000000;
    public static readonly int PICKUP_FLESH = 0x04000000;

    public static int[] stats32bit = {CS_STAT_WEIGHT_LIM, CS_STAT_SPELL_ATTUNE, CS_STAT_SPELL_REPEL, CS_STAT_SPELL_DENY, CS_STAT_EXP};
}
