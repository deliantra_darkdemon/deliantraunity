﻿/*
 * This file is part of Deliantra Unity Client.
 *
 * Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra Unity CLient is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;


public class EchoTest : MonoBehaviour
{

    public Text myTextField;
    public GameObject emptySprite;

    private String baseURL;
    private Vector3 offset;
    private int _extiNextId = 100;


    [HideInInspector]
    public int extiNext {
        get {
            return ++_extiNextId;
        }
    }

    [HideInInspector]
    public CMap getMap()
    {
        return map;
    }

    [HideInInspector]
    public void mapScrollUpdate(int px, int py)
    {
        dx += px;
        dy += py;
    }

    [HideInInspector]
    public void mapScrollReset()
    {
        dx = dy = 0;
    }

    [HideInInspector]
    public void clearMap()
    {
        map = new CMap(map.width, map.height);
    }

    [HideInInspector]
    public String password
    {
        get
        {
            return uiPassword.text;
        }
    }
    [HideInInspector]
    public String login 
    {
        get
        {
            return uiLogin.text;
        }
    }

    private List<String> lines = new List<String>();
    private static readonly int MAXLINES = 14;
    private CMap map = new CMap(12, 12);
    private FacesJSONObject facesJSON = null;
    private ReplyJSONDataArray packets = null;
    private List<Texture> t = new List<Texture>();
    [HideInInspector]
    public List<int> ids = new List<int>();

    private int index = 0;
    private int dx = 0;
    private int dy = 0;
    private WebSocket w;
    private bool simulateServer = true;
    private int numthreads = 0;
    private int ticks = 0;
    private int currentFaceNum = 0;

    private bool bWSConnected = false;


    public Text uiStats;

    [HideInInspector]
    public void updateStats()
    {
        uiStats.text = "";
        int mpMax = int.Parse(stats[DeliantraConstants.CS_STAT_MAXSP]);
        int mp = int.Parse(stats[DeliantraConstants.CS_STAT_SP]);
        int stars = (int)(((float)mp / (float)mpMax) * 30.0f);

        String bar = "[";
        for (int i = 0; i < stars; ++i)
        {
            bar += "*";
        }
        while (bar.Length < 31)
        {
            bar += " ";
        }
        bar += "]";
        uiStats.text = bar;
        /*
        foreach (String s in stats)
        {
            if (!String.IsNullOrEmpty(s))
            {
                uiStats.text += s + ",";
            }
        }
        */
    }

    #region forUnityEditor

    public InputField uiLogin;
    public InputField uiPassword;

    public GameObject[] uiElemsToDisable;
    public GameObject[] uiElemsToCycle;

    #endregion

    private int activeCycleIndex = 0;

    [HideInInspector]
    public static EchoTest instance = null;
    [HideInInspector]
    public String[] stats = new String[255];

    #region forUi
    private void setCycler()
    {
        
        int i = 0;
        foreach (GameObject o in uiElemsToCycle)
        {
            o.SetActive(i++ == activeCycleIndex);        
        }
    }

    public void onLeftArrClick()
    {
        activeCycleIndex = activeCycleIndex == 0 ? uiElemsToCycle.Length -1 : activeCycleIndex - 1;
        setCycler();
    }


    public void onRightArrClick()
    {
        activeCycleIndex = (activeCycleIndex + 1) % uiElemsToCycle.Length;
        setCycler();
    }

    public void EnterRealm ()
    {
        disableElements();
        Initialize();
    }

    private void disableElements()
    {
        foreach (GameObject o in uiElemsToDisable)
        {
            o.SetActive(false);
        }
    }
    #endregion

    // Use this for initialization
    void Initialize()
    {
        if (simulateServer == false)
        {
            String s = "";
            s+=("var WSock = window.WebSocket || window.MozWebSocket;");
            s+=("var getJSON=function(a,b,c){var d='undefined'!=typeof XMLHttpRequest?new XMLHttpRequest:new ActiveXObject('Microsoft.XMLHTTP');d.open('get',a,!0),d.onreadystatechange=function(){var a,e;4==d.readyState&&(a=d.status,200==a?(e=JSON.parse(d.responseText),b&&b(e)):c&&c(a))},d.send()};");
            s+="var ws = new WSock('ws://gameserver.deliantra.net:13327/ws');";            
            s+=("var send = function (s) { ws.send(s); };");
            s+=("var encodeIt = function (data) { var arr = []; for (var i = 0; i < data.length; ++i) { arr.push(data.charCodeAt(i));} return arr.join(',');};");
            s+=("var onOpen = function (evt) { SendMessage ('EchoTest', 'onConnect', 'foobar'); };");
            s+=("var onMessage = function (evt) { var data = evt.data; if (data instanceof Blob){ var r = new FileReader(); r.readAsBinaryString(evt.data); r.onloadend = function(progress) { SendMessage('EchoTest', 'onMessage', encodeIt(r.result)); } } else SendMessage ('EchoTest', 'onMessage', evt.data); };");
            s+=("ws.onmessage = function(evt) { onMessage(evt) };");
            s+=("getJSON('faces.json', function (o) { var s = JSON.stringify(o); SendMessage ('EchoTest', 'onFacesJSONLoaded', s); });");
            Application.ExternalEval(s);
        }else
        {
            UriBuilder ub = new UriBuilder();
            ub.Host = "gameserver.deliantra.net";
            ub.Scheme = "ws";
            ub.Port = 13327;
            ub.Path = "/ws";
            w = new WebSocket(ub.Uri);
            Debug.Log(ub.Uri);
            IEnumerator coroutine2 = onWSConnected();
            StartCoroutine(coroutine2);
        }
        //Application.ExternalEval("ws.onopen = function(evt) { onOpen(evt) };");
        //Application.ExternalEval("ws.onclose = function(evt) { onClose(evt) };");        
        //Application.ExternalEval("ws.onerror = function(evt) { onError(evt) };");        
        IEnumerator coroutine = FetchFacesJson("http://itor.pl/faces.json");
        StartCoroutine(coroutine);        
    }
    
    #region IEnumerators
    IEnumerator onWSConnected()
    {        
        yield return StartCoroutine(w.Connect());        
        bWSConnected = true;        
    }

    IEnumerator FetchFacesJson(String url)
    {
        // Start a download of the given URL
        WWW www = new WWW(url);

        // Wait for download to complete
        yield return www;
        
        onFacesJSONLoaded(www.text);
    }

    IEnumerator FetchPacketsJson(String url)
    {
        // Start a download of the given URL
        WWW www = new WWW(url);

        // Wait for download to complete
        yield return www;

        onPacketsJSONLoaded(www.text);
    }
    #endregion


    private void onPacketsJSONLoaded(String s)
    {
        packets = ReplyJSONDataArray.fromJSON(s);        
    }

    private void onFacesJSONLoaded (String s)
    {        
        facesJSON = FacesJSONObject.fromJSON(s);                
    }
    

    IEnumerator FetchImage(String url,int index)
    {
        
        numthreads++;
        // Start a download of the given URL
        WWW www = new WWW(url);
        
        // Wait for download to complete
        yield return www;
        // assign texture                
        
        t[index] = (www.texture);
        //www.Dispose();
        //www = null;
        --numthreads;
    }

    #region unitySpecial

    public void Update()
    {
        if (w != null && simulateServer == true)
        {
            if (bWSConnected == true)
            {
                byte[] data = w.Recv();

                if (data != null && data.Length > 0)
                {
                    onRawMessage(data);
                }
            }
        }


        ticks++;
        if (ticks > 14)
        {

            if (Input.GetKeyDown(KeyCode.X))
            {

                send("command invoke small bullet");
                ticks = 0;

            }
            else if (Input.GetAxis("Vertical") > 0)
            {
                send("command north");
                ticks = 0;
            }
            else if (Input.GetAxis("Vertical") < 0)
            {
                send("command south");
                ticks = 0;
            }
            else if (Input.GetAxis("Horizontal") < 0)
            {
                send("command west");
                ticks = 0;
            }
            else if (Input.GetAxis("Horizontal") > 0)
            {
                send("command east");
                ticks = 0;
            }
            else if (Input.GetKeyDown(KeyCode.Space))
            {
                send("command apply");
                ticks = 0;
            }
            else if (Input.GetKeyDown(KeyCode.Comma))
            {
                send("command get");
                ticks = 0;
            }
        }


        if (numthreads > 20)
        {
            return;
        }

        if (currentFaceNum >= ids.Count)
        {
            return;
        }

        if ((ids[currentFaceNum] < 0) || (ids[currentFaceNum] > facesJSON.data.Length))
        {
            return;
        }

        //myTextField.text = facesJSON.data[t.Count].csum;
        int id = ids[currentFaceNum];

        String csum = findCsum(id);
        if (csum == null)
        {
            return;
        }
        IEnumerator coroutine = FetchImage("http://gameserver.deliantra.net:13327/" + csum, currentFaceNum + 0);
        StartCoroutine(coroutine);
        currentFaceNum++;


    }


    public void Start()
    {
        if (instance != null)
        {
            throw new Exception("ERROR");
        }
        instance = this;
        setCycler();
    }

    public void OnGUI()
    {
        
        if (ids.Count <= 0)
        {
            return;
        }

        int index = 0;
        for (int x = 0; x< map.width; ++x)
        {
            for (int y = 0; y<map.height; ++y)
            {
                MapCellData data = map.getCell(x, y).data;
                

                for (int i = 0; i <= 2; ++i)
                {
                    
                    if (data.tiles[i] == 0)
                    {
                        continue;
                    }
                    index = getTextureIndex(data.tiles[i]);
                    if (index < 0)
                    {
                        //Debug.Log("not found: " + map.getCell(x, y).data.tiles[i].ToString());
                        continue;
                    }
                    if ( t[index] != null)
                    {
                        GUI.DrawTexture(new Rect(x * 64, y * 64, 64, 64), t[index]);
                    }else
                    {
                        //Debug.Log("null at " + index.ToString());
                    }
                }
                
                
                
            }
        }
        
    }
    #endregion

    private int getTextureIndex(int index)
    {
        int i = 0;
        foreach (int id in ids){
            if (id == index)
            {
                return i;
            }
            ++i;
        }
        return -1;
    }

    
    private int findIndex(MapCellData data, int layer)
    {
        if (layer>=0 && layer < data.tiles.Length)
            return data.tiles[layer];
        return -1;
    }

    private void onConnect()
    {        
        send("version {\"clientver\":\"0.001\",\"client\":\"deliantra html5 client\",\"osver\":\"linux\",\"protver\":1}");
    }

    private void send(String s)
    {

        if (simulateServer)
        {            
            if (!bWSConnected)
            {
                return;
            }
            w.SendString(s);
            return;
        }
        Application.ExternalEval("send('" + s + "');");
    }

    #region utils
    private object GetInstance(string strFullyQualifiedName)
    {
        Type t = Type.GetType(strFullyQualifiedName);
        if (t == null)
        {
            Debug.Log(strFullyQualifiedName);
            return null;
        }
        return Activator.CreateInstance(t);
    }



    private string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1);
    }

    #endregion

    public void setBaseURL(string v)
    {
        baseURL = v;
    }


    private void onRawMessage(byte [] bytes)
    {
        String message = System.Text.Encoding.ASCII.GetString(bytes);
        String replyToFeed = null;
     
        //if (lines.Count > MAXLINES)
        //{
        //    lines.RemoveAt(0);
        //}
        //lines.Add("\n< -- " + message);
        
        String className = "Feed" + prettySuffix(message.Contains(" ") ? message.Split(' ')[0] : message);
        Debug.Log(className);
        
        Feed f = (Feed)GetInstance(className);
        if (f == null)
        {
            return;
        }

        f.setData(message, bytes);
        f.process(out replyToFeed);

        if (!String.IsNullOrEmpty(replyToFeed))
        {
            send(replyToFeed);
        }
        return;
    }

    private string prettySuffix(string name)
    {
        string r = UppercaseFirst(name);
        if (r.Contains("_"))
        {
            String[] parts = r.Split('_');
            String output = "";
            foreach (String part in parts)
            {
                output += UppercaseFirst(part);
            }
            return output;
        }
        return r;
    }

    private void onMessage(String encodedMessage)
    {

        String[] charCodes = encodedMessage.Split(',');
        byte[] bytes = new byte[charCodes.Length];
        for (int i = 0; i < bytes.Length; ++i)
        {
            int Byte = int.Parse(charCodes[i]);
            bytes[i] = (byte)Byte;
        }
        onRawMessage(bytes);
    }
        
    [HideInInspector]
    public void scroll_map()
    {
        
        int tmpDx = dx;
        int tmpDy = dy;
        var i = 0;
        int mapWidth = map.width;
        int mapHeight = map.height;
        while (i < tmpDy)
        {
            map.rows.RemoveAt(0);
            map.rows.Add(new MapRow(mapWidth));
            i++;
        }
        i = 0;
        while (tmpDy < i)
        {
            map.rows.Insert(0, new MapRow(mapWidth));
            map.rows.RemoveAt(map.rows.Count - 1);
            --i;
        }
        i = 0;
        while (i < tmpDx)
        {
            for (int j = 0; j < mapHeight; ++j)
            {
                map.rows[j].cells.RemoveAt(0);
                map.rows[j].cells.Add(new MapCell());
            }
            i++;
        }
        i = 0;
        while (tmpDx < i)
        {
            
            for (int j = 0; j < mapHeight; ++j)
            {
                map.rows[j].cells.RemoveAt(map.rows[j].cells.Count-1);
                map.rows[j].cells.Insert(0,new MapCell());
            }
            i--;
        }

    }

    
    public void need_facenum(int face)
    {

        ids.Add(face);
        t.Add(null);
        //Debug.Log(face);     
    }

    

    private string findCsum(int id)
    {
        for (int i = 0; i < facesJSON.data.Length; ++i)
        {
            if (facesJSON.data[i].id == id)
            {
                return facesJSON.data[i].csum;
            }
        }
        return null;
    }

    public string getMapSizeAsString()
    {
        return map.width.ToString() + "x" + map.height.ToString();
    }
}
