﻿/*
 * This file is part of Deliantra Unity Client.
 *
 * Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra Unity CLient is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class CMap
{
    public List<MapRow> rows = new List<MapRow>();

    public int width
    {
        get
        {
            if (rows.Count == 0)
            {
                return 0;
            }
            return rows[0].length;
        }
    }

    public int height
    {
        get
        {
            return rows.Count;
        }
    }

    public void addRow(int size)
    {
        rows.Add(new MapRow(size));
    }

    public CMap(int w, int h)
    {
        while (height < h)
        {
            addRow(w);
        }
    }

    public MapCell getCell(int x, int y)
    {
        if (x < width && y < height)
        {
            return rows[y].cells[x];
        }
        return null;
    }

    public String serialize()
    {
        String s = "";

        for (int y = 0; y < height; ++y)
        {
            for (int x = 0; x < width; ++x)
            {
                MapCell c = getCell(x, y);
                String t = c.data.darkness.ToString();
                while (t.Length < 3)
                {
                    t = "0" + t;
                }
                s += t;
            }
            s += "\n";
        }
        return s;
    }
}