﻿/*
 * This file is part of Deliantra Unity Client.
 *
 * Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra Unity CLient is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class FeedStats : BaseFeed
{
    public override bool process(out String reply)
    {
        bool r = base.process(out reply);
        String[] stats = biggie.stats;

        PerlUnpacker pu = new PerlUnpacker(data, 6);
        while (pu.hasMore())
        {

            int stat = pu.unpack_Uint8();
            int intVal = 0;
            double doubleVal = 0;

            bool is32bit = false;
            foreach (int stat32 in DeliantraConstants.stats32bit)
            {
                if (stat32 == stat)
                {
                    is32bit = true;
                    break;
                }
            }
            if (is32bit)
            {
                intVal = pu.unpack_int32();
                stats[stat] = intVal.ToString();
                continue;
            }
            else if (stat == DeliantraConstants.CS_STAT_SPEED || stat == DeliantraConstants.CS_STAT_WEAP_SP)
            {
                doubleVal = (1 / DeliantraConstants.FLOAT_MULTF) * pu.unpack_int32();
                stats[stat] = doubleVal.ToString();
            }
            else if (stat == DeliantraConstants.CS_STAT_RANGE || stat == DeliantraConstants.CS_STAT_TITLE)
            {
                stats[stat] = pu.unpack_string(pu.unpack_Uint8());
            }
            else if (stat == DeliantraConstants.CS_STAT_EXP64)
            {
                int hi = pu.unpack_int32();
                int lo = pu.unpack_int32();
                stats[stat] = (hi * Math.Pow(2, 32) + lo).ToString();
            }
            else
            {
                stats[stat] = pu.unpack_Uint16().ToString();
            }
        }
        biggie.updateStats();
        reply = null;
        return false;        
    }
}