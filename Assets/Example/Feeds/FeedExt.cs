﻿/*
 * This file is part of Deliantra Unity Client.
 *
 * Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra Unity CLient is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class FeedExt : BaseFeed
{
    public override bool process(out String reply)
    {
        int replyid = 0;
        bool r = base.process(out reply);
        if (!r)
        {
            reply = null;
            return false;
        }

        if (!this.sData.Contains("reply"))
        {
            reply = null;
            return false;
        }         

        ReplyJSONDataArray jsonreply = ReplyJSONDataArray.fromDeliantraJSON(sData.Substring(4));
        String[] info = jsonreply.data;
        if (!info[0].Contains("-"))
        {
            reply = null;
            return false;
        }
        
    
        if (!int.TryParse(info[0].Split('-')[1], out replyid))
        {
            reply = null;
            return false;
        }

        /*
        if (info.Length > 1)
        {
            int[] resourcesid = null;

            if (replyid > 100)
            {
                resourcesid = new int[info.Length - 1];
                for (int i = 1; i < info.Length; ++i)
                {
                    resourcesid[i - 1] = int.Parse(info[i]);
                }
            }

        }*/

        switch (replyid)
        {
            case 100:
                biggie.setBaseURL("http://gameserver.deliantra.net:13327" + info[1] + "");
                reply = ("exti [\"resource\"," + biggie.extiNext.ToString() + ",\"exp_table\"]");
                return true;
            case 101:
                reply = "exti [\"resource\"," + biggie.extiNext.ToString() + ", \"skill_info\", \"spell_paths\", \"command_help\"]";
                return true;
            case 102:
                reply = "addme";
                return true;
            default:
                reply = null;
                return false;
        }
        reply = null;
        return false;
    }
}
    

