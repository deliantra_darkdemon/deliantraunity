﻿/*
 * This file is part of Deliantra Unity Client.
 *
 * Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra Unity CLient is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class FeedMap1a : BaseFeed
{
    public override bool process(out String reply)
    {
        bool r = base.process(out reply);
        biggie.scroll_map();
        biggie.mapScrollReset();
        int index = 6;
        int cmd = 0;
        int ext = 0;
        CMap map = biggie.getMap();

        while (index < data.Length - 2)
        {
            int flags = (data[index++] << 8);
            flags += data[index++];
            int x = ((flags >> 10) & 63);
            int y = ((flags >> 4) & 63);
            cmd = 0;
            ext = 0;
            
            MapCell cell = map.getCell(x, y);
            if ((flags & 15) == 0) 
            {
                cell.data.clear();
                continue;
            }
            if (cell.data.darkness == 0)
            {
                cell.data.darkness = 256;                
            }

            if ((flags & 8) != 0)
            {
                do
                {

                    ext = data[index++];
                    cmd = ext & 0x7f;
                    if (cmd < 4)
                    {
                        cell.data.darkness = 255 - ext * 64 + 1;
                        cell.data.isDirty = true;
                    }
                    else if (cmd == 5)
                    {
                        cell.data.stat_width = 1;
                        cell.data.stat_hp = data[index++];
                        cell.data.isDirty = true;
                    }
                    else if (cmd == 6)
                    {
                        cell.data.stat_width = data[index++] + 1;
                        cell.data.isDirty = true;
                    }
                    else if (cmd == 0x47)
                    {
                        if (data[index] == 1) cell.data.player = data[index + 1];
                        else if (data[index] == 2) cell.data.player = data[index + 2] + (data[index + 1] << 8);
                        else if (data[index] == 3) cell.data.player = data[index + 3] + (data[index + 2] << 8) + (data[index + 1] << 16);
                        else if (data[index] == 4) cell.data.player = data[index + 4] + (data[index + 3] << 8) + (data[index + 2] << 16) + (data[index + 1] << 24);

                        index += data[index] + 1;
                    }
                    else if (cmd == 8) // cell flags
                        cell.data.flags = data[index++];
                    else if ((ext & 0x40) != 0) // unknown, multibyte => skip
                        index += data[index] + 1;
                    else
                        index++;
                } while ((ext & 0x80) != 0);
            }


            cell.data.tilesCount = 0;
            for (int z = 0; z <= 2; ++z)
                if ((flags & (4 >> z)) != 0)
                {
                    int face = (data[index++] << 8);
                    face += data[index++];
                    if (face != 0 && (biggie.ids.Contains(face) == false))
                    {
                        biggie.need_facenum(face);
                    }
                    cell.data.tiles[z] = face;
                    cell.data.isDirty = true;
                    //cell.data.tilesCount = z;
                }
        }

        reply = null;
        return false;
    }
}