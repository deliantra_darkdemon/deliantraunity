﻿/*
 * This file is part of Deliantra Unity Client.
 *
 * Copyright (©) 2017 Marek Olszewski <m.olszewski@wit.edu.pl>
 *
 * Deliantra Unity CLient is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>
 */
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PerlUnpacker
{
    private byte[] bytes = null;
    public int offset = 0;
    private int v;

    public int length
    {
        get
        {
            return bytes.Length;
        }
    }

    public PerlUnpacker(byte[] bytes, int offset)
    {
        this.bytes = bytes;
        this.offset = offset;
    }

    public PerlUnpacker(string s, int offset)
    {
        bytes = System.Text.Encoding.ASCII.GetBytes(s);
        this.offset = offset;
    }

    public int unpack_w()
    {
        int num = 0;
        int bt = 0;
        do
        {
            num *= 128;
            bt = getByte();
            num += (bt & 0x7f);

        } while ((bt & 0x80) > 0);


        return convert(num);
    }

    public int unpack_int32()
    {
        int ret = 0;
        for (var i = 0; i < 4; ++i)
        {
            ret *= 256;
            ret += bytes[offset++];
        }
        return ret;
    }

    public int convert(int a)
    {
        int ret = 0;

        while (a > 0)
        {
            ret *= 10;
            ret += a % 10;
            a /= 10;
        }

        return ret;
    }
    public String unpack_CSlashA()
    {
        int len = unpack_Uint8();

        String output;
        String ret = "";
        for (int i = 0; i < len; ++i)
        {
            int c = unpack_Uint8();

            output = "0123456789abcdef"[c >> 4].ToString();
            output = ("0123456789abcdef"[c & 0xf].ToString()) + output;
            ret += output;
            //Debug.Log(Convert.ToInt32("3A", 16));

        }
        return ret;
    }

    internal string unpack_string(int v)
    {
        String ret = System.Text.Encoding.ASCII.GetString(bytes, this.offset, v);
        offset += v;
        return ret;
    }

    public int unpack_Uint8()
    {
        int ret = 0;
        for (int i = 0; i < 1; ++i)
        {
            ret *= 256;
            ret += getByte();
        }

        return ret;


    }

    public int unpack_Uint16()
    {
        int ret = 0;
        for (int i = 0; i < 2; ++i)
        {
            ret *= 256;
            ret += getByte();
        }
        return ret;
    }

    private int getByte()
    {
        return (int)bytes[offset++];
    }

    public bool hasMore()
    {
        return offset < length;
    }

    public String serialize()
    {
        String s = "";
        for (int i = 0; i < length; ++i)
        {
            s += bytes[i].ToString() + ",";
        }
        return s.Substring(0, s.Length - 1);
    }
}
