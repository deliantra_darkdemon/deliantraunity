### Deliantra Unity Client

## What is Deliantra?

Deliantra is a thrilling MMORPG in a retro, pixel art world where magic and monsters abound.

Deliantra is free to download, and free to play. There aren't any premium accounts, and you don't have to pay money to get decent equipment. This free software (some say 'open source') game was built by players like you, and is made available free of charge because we love to play it, and hope that you will too. Please continue your tour of Deliantra by clicking on the arrows above.

## Why this project?

Official Deliantra client is pretty old and does not work perfectly under windows 10 for example.

This is an attempt to make more modern client for Deliantra.

## Links

http://www.deliantra.net

http://cvs.schmorp.de

## Test it

http://itor.pl/

http://itor.pl/build/win/deliantra.zip

http://itor.pl/build/lin/deliantra.zip


License
=======
 
 * Deliantra Unity Client is free software: you can redistribute it and/or
 * modify it under the terms of the Affero GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the Affero GNU General Public Licens
 * and the GNU General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * The authors can be reached via e-mail to <support@deliantra.net>